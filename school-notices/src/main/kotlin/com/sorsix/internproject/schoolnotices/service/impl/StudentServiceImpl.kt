package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Course
import com.sorsix.internproject.schoolnotices.domain.Student
import com.sorsix.internproject.schoolnotices.domain.Teacher
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDetailsDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.NewStudentRequest
import com.sorsix.internproject.schoolnotices.repository.CourseRepository
import com.sorsix.internproject.schoolnotices.repository.StudentRepository
import com.sorsix.internproject.schoolnotices.repository.TeacherRepository
import com.sorsix.internproject.schoolnotices.service.StudentService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@Service
class StudentServiceImpl(private val studentRepository: StudentRepository,
                         private val teacherRepository: TeacherRepository,
                         private val courseRepository: CourseRepository) : StudentService {

    var formatter = DateTimeFormatter.ofPattern("d/MM/yyyy")

    val logger: Logger = LoggerFactory.getLogger(StudentServiceImpl::class.java)

    override fun createNewUser(newStudent: NewStudentRequest) {
        val localDate: LocalDate = LocalDate.parse(newStudent.studentDOB, formatter)
        val student: Student = Student(newStudent.studentEMBG, newStudent.studentName, localDate, newStudent.studentSchoolYear, 0.0,LocalDate.now())
        newStudent.coursesTeachers.forEach { it ->
            run {
                val teacher = teacherRepository.findById(it.teacherId).orElseThrow()
                val course = courseRepository.findById(it.courseId).orElseThrow()
                if (student.teachers.isNullOrEmpty()) {
                    val teacherList = mutableListOf<Teacher>(teacher)
                    student.teachers = teacherList
                } else {
                    if(!student.teachers!!.contains(teacher)){
                        student.teachers!!.add(teacher)
                    }
                }
                if (student.courses.isNullOrEmpty()) {
                    val courseList = mutableListOf<Course>(course)
                    student.courses = courseList
                } else {
                    student.courses!!.add(course)
                }
                studentRepository.save(student)
            }
        }
        logger.info("Student [{}] created",student)
    }

    override fun getStudentById(id: String): StudentDetailsDTO {
        val student = studentRepository.findById(id).orElseThrow()
        val studentTeachers = student.teachers?.map { it -> it.teacherUser?.firstName + it.teacherUser?.lastName }?.toMutableList()
        val studentCourses = student.courses?.map { it -> it.name }?.toMutableList()
        val studentParents = student.parents?.map{it -> it.parentUser.firstName + it.parentUser.lastName}?.toMutableList()
        val studentDetailsDTO = StudentDetailsDTO(student.EMBG, student.name, student.dateOfBirth, student.schoolYear, student.gpa,student.dateEnrolled, studentTeachers, studentCourses,studentParents)
        return studentDetailsDTO;

    }

    override fun deleteStudent(id: String) {
        this.studentRepository.deleteById(id)
        logger.info("Student with id [{}] deleted",id)
    }
}