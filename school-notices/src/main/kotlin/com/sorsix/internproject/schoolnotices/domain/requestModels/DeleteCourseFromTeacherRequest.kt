package com.sorsix.internproject.schoolnotices.domain.requestModels

data class DeleteCourseFromTeacherRequest(
        val teacherId: Long,
        val courseId :Long
)