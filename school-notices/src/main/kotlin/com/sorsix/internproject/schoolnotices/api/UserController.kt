package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.User
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateUserRequest
import com.sorsix.internproject.schoolnotices.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/user")
class UserController (private val userService: UserService){

    @PostMapping("/create")
    fun createUser(@RequestBody userRequest: CreateUserRequest) {
        this.userService.createNewUser(userRequest)
    }

    @GetMapping("/{id}")
    fun getUserById(@PathVariable id:String):User{
        return this.userService.getUserById(id)
    }

    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable id:String){
        this.userService.deleteUser(id)
    }

    @PutMapping("/update")
    fun updateUser(@RequestBody user: User){
        this.userService.updateUser(user)
    }
}