package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "students")
data class Student(
        @Id
        val EMBG: String,
        val name: String,
        val dateOfBirth: LocalDate,
        val schoolYear: String,
        var gpa: Double,
        val dateEnrolled: LocalDate,
        @ManyToMany
        @JsonIgnore
        var parents: MutableList<Parent>? = null,
        @ManyToMany
        @JsonIgnore
        var teachers: MutableList<Teacher>? = null,
        @ManyToMany
        @JsonIgnore
        var courses: MutableList<Course>? = null,
        @OneToMany(mappedBy = "student")
        val grades: MutableList<Grade>? = null

)