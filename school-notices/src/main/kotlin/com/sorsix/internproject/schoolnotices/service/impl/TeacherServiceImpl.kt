package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Course
import com.sorsix.internproject.schoolnotices.domain.Teacher
import com.sorsix.internproject.schoolnotices.domain.modelDtos.TeacherDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.AddCoursesToTeacherRequest
import com.sorsix.internproject.schoolnotices.domain.requestModels.CourseAddRequest
import com.sorsix.internproject.schoolnotices.domain.requestModels.DeleteCourseFromTeacherRequest
import com.sorsix.internproject.schoolnotices.repository.CourseRepository
import com.sorsix.internproject.schoolnotices.repository.TeacherRepository
import com.sorsix.internproject.schoolnotices.service.TeacherService
import javassist.NotFoundException
import org.hibernate.annotations.NotFound
import org.springframework.stereotype.Service
import java.util.*
import kotlin.reflect.jvm.internal.impl.serialization.deserialization.FlexibleTypeDeserializer

@Service
class TeacherServiceImpl(private val teacherRepository: TeacherRepository, private val courseRepository: CourseRepository) : TeacherService {
    override fun getTeachersByName(teacherName: Optional<String>): List<TeacherDTO> {
        return teacherRepository.findByName(teacherName.orElse(""))

    }
    override fun addAllCoursesToTeacher(teacherRequest: AddCoursesToTeacherRequest) {
        val teacher = this.teacherRepository.findById(teacherRequest.teacherId).orElseThrow()
        teacherRequest.courses.forEach { it ->
            run {
                val course = courseRepository.findById(it.courseId).orElseThrow()
                if (course.teachersByCourse.isNullOrEmpty()) {
                    val teacherList = mutableListOf<Teacher>(teacher)
                    courseRepository.save(Course(course.id, course.name, course.credits, teachersByCourse = teacherList))
                } else {
                    val teacherList = course.teachersByCourse
                    if (!teacherList!!.contains(teacher)){
                        teacherList.add(teacher)
                        courseRepository.save(Course(course.id, course.name, course.credits, teachersByCourse = teacherList))
                    }else {
                        return@forEach
                    }
                }
            }
        }
    }

    override fun getTeacherById(id: String): TeacherDTO {
        return teacherRepository.findById(id.toLong())
                .map { it ->
                    TeacherDTO(it.id,
                            it.code,
                            it.teacherUser?.id ?: throw NotFoundException("Teacher user not found"),
                            it.teacherUser.firstName,
                            it.teacherUser.lastName,
                            it.teacherUser.email)
                }
                .orElseThrow { NotFoundException("Teacher id not found") }
    }

    override fun deleteTeacher(id: String) {
        teacherRepository.deleteById(id.toLong())
    }

    override fun deleteCourseFromTeacher(deleteCourseFromTeacherRequest: DeleteCourseFromTeacherRequest) {
        val teacher = this.teacherRepository.findById(deleteCourseFromTeacherRequest.teacherId).orElseThrow()
        val course = courseRepository.findById(deleteCourseFromTeacherRequest.courseId).orElseThrow()
        course.teachersByCourse?.remove(teacher)
        courseRepository.save(course)

    }

}