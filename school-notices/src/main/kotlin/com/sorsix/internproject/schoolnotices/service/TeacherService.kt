package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.modelDtos.TeacherDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.AddCoursesToTeacherRequest
import com.sorsix.internproject.schoolnotices.domain.requestModels.DeleteCourseFromTeacherRequest
import java.util.*

interface TeacherService {
    fun getTeachersByName(teacherName: Optional<String>): List<TeacherDTO>

    fun addAllCoursesToTeacher(teacherRequest: AddCoursesToTeacherRequest)

    fun getTeacherById(id:String):TeacherDTO

    fun deleteTeacher(id:String)

    fun deleteCourseFromTeacher(deleteCourseFromTeacherRequest: DeleteCourseFromTeacherRequest)

}