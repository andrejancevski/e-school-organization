package com.sorsix.internproject.schoolnotices.domain.requestModels

data class AddCoursesToTeacherRequest(
        val teacherId: Long,
        val courses: MutableList<CourseAddRequest>
){}
