package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.Notice
import com.sorsix.internproject.schoolnotices.domain.Page
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDetailDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateNoticeRequest
import org.springframework.web.multipart.MultipartFile

interface NoticeService {
    fun createNewNotice(file: MultipartFile, notice: CreateNoticeRequest) {}

    fun getAllNotices(page: Int, size: Int): Page<NoticeDTO?>

    fun getFullNoticeById(id:String):NoticeDetailDTO


    fun deleteNotice(id:String)

    // fun updateNotice(notice:NoticeDTO)
}