package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name="teachers")
data class Teacher(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val code: String,
        @OneToOne(cascade = arrayOf(CascadeType.ALL))
        @JsonIgnore
        @JoinColumn(name="teacher_id", referencedColumnName = "id")
        val teacherUser: User?,
        @ManyToMany(mappedBy = "teachers")
        var students: MutableList<Student>? = null,
        @ManyToMany(mappedBy = "teachersByCourse", cascade = arrayOf(CascadeType.ALL))
        var courses: MutableList<Course>? = null,
        @OneToMany(mappedBy = "teacher")
        @JsonIgnore
        val grades: MutableList<Grade>? = null,
        @OneToMany(mappedBy = "teacherMeeting")
        val meetings: MutableList<Meeting>? = null



)