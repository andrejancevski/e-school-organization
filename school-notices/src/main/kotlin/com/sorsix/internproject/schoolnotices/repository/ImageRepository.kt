package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.ImageModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface ImageRepository : JpaRepository<ImageModel, Long> {

}