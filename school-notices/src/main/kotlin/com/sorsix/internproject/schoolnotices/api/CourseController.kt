package com.sorsix.internproject.schoolnotices.api


import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseTeacherDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO
import com.sorsix.internproject.schoolnotices.service.CourseService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/course")
class CourseController(private val courseService: CourseService) {

    @GetMapping("/allCourses")
    fun getCourses(): List<CourseDTO>{
        return this.courseService.getAllCourses()
    }

    @GetMapping("/coursesTeachers")
    fun getAllTeachersWithCourses(): List<CourseTeacherDTO> {
        return this.courseService.getAllCoursesWithTeachers()
    }

    @GetMapping("/coursesTeacher")
    fun getAllCoursesForTeacher(@RequestParam teacherEmail: String): List<CourseDTO>? {
        return this.courseService.getCoursesForTeacher(teacherEmail)
    }

    @GetMapping("/students")
    fun getAllStudentsFromCourse(@RequestParam courseId: Long): MutableList<StudentDTO>?{
        return this.courseService.getAllStudentsFromCourse(courseId);
    }

    @GetMapping("/student/{studentId}")
    fun getCoursesFromStudent(@PathVariable studentId: String): List<CourseDTO>? {
       return this.courseService.getCourseFromStudent(studentId)
    }



}