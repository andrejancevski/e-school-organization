package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CoursesTeachersRequest(
        val courseId: Long,
        val teacherId: Long,
        val teacherCode:String
)