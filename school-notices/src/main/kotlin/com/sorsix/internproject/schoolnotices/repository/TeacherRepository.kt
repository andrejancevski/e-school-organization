package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.Teacher
import com.sorsix.internproject.schoolnotices.domain.modelDtos.TeacherDTO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TeacherRepository : JpaRepository<Teacher, Long> {

    @Query("select new com.sorsix.internproject.schoolnotices.domain.modelDtos.TeacherDTO(t.id,t.code,t.teacherUser.id,t.teacherUser.firstName,t.teacherUser.lastName,t.teacherUser.email) from Teacher t where t.teacherUser.firstName like %?1% ")
    fun findByName(name: String): List<TeacherDTO>

}