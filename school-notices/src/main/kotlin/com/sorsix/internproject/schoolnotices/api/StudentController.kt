package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDetailsDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.NewStudentRequest
import com.sorsix.internproject.schoolnotices.service.StudentService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/student")
class StudentController (private val studentService: StudentService){

    @PostMapping("/create")
    fun createUser(@RequestBody studentRequest: NewStudentRequest) {
        this.studentService.createNewUser(studentRequest)
    }

    @GetMapping("/{id}")
    fun getStudentById(@PathVariable id:String): StudentDetailsDTO {
        return this.studentService.getStudentById(id)
    }

    @DeleteMapping("/{id}")
    fun deleteStudent(@PathVariable id:String){
        this.studentService.deleteStudent(id)
    }
}