package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.Page
import com.sorsix.internproject.schoolnotices.domain.modelDtos.GradeDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateStudentGradeRequest

interface GradeService {

    fun createNewGrade(createStudentGradeRequest: CreateStudentGradeRequest) {}

    fun getGradesForStudent(page: Int, size: Int, studentId: String): Page<GradeDTO>?
}