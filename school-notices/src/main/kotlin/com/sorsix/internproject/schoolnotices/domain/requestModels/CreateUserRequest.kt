package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CreateUserRequest(val firstName: String,
                             val lastName: String,
                             val email: String,
                             val password: String,
                             val teacherCode: String,
                             val studentsEMBG: List<String>,
                             val role: String)