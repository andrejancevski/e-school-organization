package com.sorsix.internproject.schoolnotices.domain.modelDtos

data class StudentDTO(
        val studentEMBG: String,
        val studentName: String
)