package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CreateMeetingRequest (
        val studentId: String,
        val reasonForMeeting: String,
        val location: String,
        val dateTime: String
)