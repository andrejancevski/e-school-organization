package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name="parents")
data class Parent(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        @OneToOne(cascade = arrayOf(CascadeType.ALL))
        @JoinColumn(name="parent_id", referencedColumnName = "id")
        val parentUser: User,
        @ManyToMany(mappedBy = "parents")
        val students: MutableList<Student>? = null,
        @OneToMany(mappedBy = "parent")
        val meetings: MutableList<Meeting>? = null,
        @OneToMany(mappedBy = "parentAbsence")
        val absences: MutableList<Absence>? = null
)