package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.Absence
import com.sorsix.internproject.schoolnotices.domain.modelDtos.AbsenceDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateAbsenceRequest

interface AbsenceService {

    fun createNewAbsence(newAbsenceRequest: CreateAbsenceRequest)

    fun getAbsences():List<AbsenceDTO>
}