package com.sorsix.internproject.schoolnotices.domain.modelDtos


class AbsenceDTO (
        val id : Long,
        val reason: String,
        val startDate: String,
        val endDate: String,
        val absentStudent: String,
        val parentName:String
)