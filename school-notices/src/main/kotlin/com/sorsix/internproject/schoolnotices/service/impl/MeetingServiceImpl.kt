package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Meeting
import com.sorsix.internproject.schoolnotices.domain.modelDtos.MeetingDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateMeetingRequest
import com.sorsix.internproject.schoolnotices.exceptions.StudentNotFoundException
import com.sorsix.internproject.schoolnotices.exceptions.UserNotFoundException
import com.sorsix.internproject.schoolnotices.repository.MeetingRepository
import com.sorsix.internproject.schoolnotices.repository.StudentRepository
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import com.sorsix.internproject.schoolnotices.service.MeetingService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class MeetingServiceImpl(private val meetingRepository: MeetingRepository,
                         private val userRepository: UserRepository,
                         private val studentRepository: StudentRepository) : MeetingService {

    var formatter = DateTimeFormatter.ofPattern("d/MM/yyyy HH:mm")

    val logger: Logger = LoggerFactory.getLogger(MeetingServiceImpl::class.java)

    override fun createNewMeeting(meeting: CreateMeetingRequest) {
        val userName = SecurityContextHolder.getContext().authentication.name
        val student = this.studentRepository.findById(meeting.studentId).orElseThrow { StudentNotFoundException("This student does not exist") }
        val user = this.userRepository.findByEmail(userName).orElseThrow { UserNotFoundException("This user does not exist") }
        val localDateTime = LocalDateTime.parse(meeting.dateTime, formatter)
        student.parents?.forEach { it ->
            run {
                meetingRepository.save(Meeting(0, meeting.location, meeting.reasonForMeeting, localDateTime, LocalDate.now(), user.teacher!!, it))
            }
        }
        logger.info("Meeting [{}] created",meeting)
    }

    override fun getAllMeetings(): MutableList<MeetingDTO>? {
        val userName = SecurityContextHolder.getContext().authentication.name
        val roles = SecurityContextHolder.getContext().authentication.authorities.toMutableList()
        val user = this.userRepository.findByEmail(userName).orElseThrow { UserNotFoundException("User with this email is not logged or does not exist") }
        if (roles.get(0).authority === "ROLE_TEACHER") {
            return user.teacher?.meetings?.map { it ->
                MeetingDTO(it.id, it.location, it.reason, it.dateTime, it.dateCreated, "",
                        it.parent.parentUser.firstName + ' ' + it.parent.parentUser.lastName)
            }?.toMutableList();
        } else {
            return user.teacher?.meetings?.map { it ->
                MeetingDTO(it.id, it.location, it.reason, it.dateTime, it.dateCreated,
                        it.teacherMeeting.teacherUser?.firstName + ' ' + it.teacherMeeting.teacherUser?.lastName,
                        "")
            }?.toMutableList();
        }
    }


}