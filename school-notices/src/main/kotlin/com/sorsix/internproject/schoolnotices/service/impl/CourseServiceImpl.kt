package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseTeacherDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO
import com.sorsix.internproject.schoolnotices.repository.CourseRepository
import com.sorsix.internproject.schoolnotices.repository.StudentRepository
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import com.sorsix.internproject.schoolnotices.service.CourseService
import org.springframework.stereotype.Service

@Service
class CourseServiceImpl (private val courseRepository: CourseRepository,
                         private val userRepository: UserRepository,
                         private val studentRepository: StudentRepository) : CourseService{
    override fun getAllCourses(): List<CourseDTO> {
       return  this.courseRepository.findAllBy()
    }

    override fun getAllCoursesWithTeachers(): List<CourseTeacherDTO> {
        return this.courseRepository.getAllCoursesWithTeachers()
    }

    override fun getCoursesForTeacher(teacherEmail: String): List<CourseDTO>? {
        val teacher = this.userRepository.findByEmail(teacherEmail).orElseThrow()
        return teacher.teacher?.courses?.map { it -> CourseDTO(it.id,it.name,it.credits) }
    }

    override fun getAllStudentsFromCourse(courseId: Long): MutableList<StudentDTO>? {
        val course = this.courseRepository.findById(courseId).orElseThrow()
        return course.students?.map { it -> StudentDTO(it.EMBG,it.name) }?.toMutableList();
    }

    override fun getCourseFromStudent(studentId: String): List<CourseDTO>? {
        val student = studentRepository.findById(studentId).orElseThrow()
        return student.courses?.map{it -> CourseDTO(it.id,it.name,it.credits)}?.toMutableList();
    }
}