package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name="courses")
data class Course(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val name: String,
        val credits: Int,
        @ManyToMany(mappedBy = "courses")
        var students: MutableList<Student>? = null,
        @ManyToMany(cascade = arrayOf(CascadeType.ALL))
        @JsonIgnore
        var teachersByCourse: MutableList<Teacher>? = null,
        @OneToMany(mappedBy = "course")
        @JsonIgnore
        val grades: MutableList<Grade>? = null

)