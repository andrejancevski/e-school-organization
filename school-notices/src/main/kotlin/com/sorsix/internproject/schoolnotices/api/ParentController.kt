package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.Student
import com.sorsix.internproject.schoolnotices.domain.modelDtos.ParentDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO
import com.sorsix.internproject.schoolnotices.service.ParentService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/parents")
class ParentController(private val parentService: ParentService) {

    @GetMapping("parentStudents")
    fun getStudentFromParents(): MutableList<StudentDTO>? {
        return this.parentService.getChildrenFromParents()
    }

    @GetMapping("/student/{studentId}")
    fun getParentsFromStudent(@PathVariable studentId: String): List<ParentDTO>? {
        return this.parentService.getParentsFromStudent(studentId)
    }
}
