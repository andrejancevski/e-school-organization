package com.sorsix.internproject.schoolnotices.domain.modelDtos

data class CourseDTO(
        val courseId: Long,
        val courseName: String,
        val courseCredits: Int
)