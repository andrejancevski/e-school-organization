package com.sorsix.internproject.schoolnotices.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(HttpStatus.BAD_REQUEST)
class ParentsNotRegisteredException (val errorMessage: String): RuntimeException(errorMessage){
    override fun toString(): String {
        return this.errorMessage
    }
}