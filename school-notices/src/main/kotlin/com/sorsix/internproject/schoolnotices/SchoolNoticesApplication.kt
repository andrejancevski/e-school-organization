package com.sorsix.internproject.schoolnotices

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SchoolNoticesApplication

fun main(args: Array<String>) {
    runApplication<SchoolNoticesApplication>(*args)
}
