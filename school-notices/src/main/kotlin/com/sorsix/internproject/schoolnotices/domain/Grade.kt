package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name="grades")
data class Grade(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val name: String,
        val gradingPeriod: String,
        val type: String,
        val date: LocalDate,
        val points: Int,
        val review: String,
        val forGPA: Boolean,
        @ManyToOne
        @JsonIgnore
        val student: Student,
        @ManyToOne
        val course: Course,
        @ManyToOne
        val teacher: Teacher?

)