package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.Course
import com.sorsix.internproject.schoolnotices.domain.Teacher
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseTeacherDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.TeacherDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.AddCoursesToTeacherRequest
import com.sorsix.internproject.schoolnotices.domain.requestModels.DeleteCourseFromTeacherRequest
import com.sorsix.internproject.schoolnotices.repository.CourseRepository
import com.sorsix.internproject.schoolnotices.service.TeacherService
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/teacher")
class TeacherController(private val teacherService: TeacherService) {

    @GetMapping
    fun getTeachersByName(@RequestParam name: Optional<String>): List<TeacherDTO> {
        return this.teacherService.getTeachersByName(name)
    }

    @PostMapping("/addCourses")
    fun addCoursesForTeacher(@RequestBody addCoursesToTeacherRequest: AddCoursesToTeacherRequest) {
        this.teacherService.addAllCoursesToTeacher(addCoursesToTeacherRequest)
    }

    @GetMapping("/{id}")
    fun getTeacherById(@PathVariable id:String):TeacherDTO{
        return teacherService.getTeacherById(id)
    }

    @DeleteMapping("/{id}")
    fun deleteTeacher(id:String){
        teacherService.deleteTeacher(id)
    }

    @DeleteMapping("/removeCourse")
    fun deleteCourseFromTeacher(@RequestBody deleteCourseFromTeacherRequest: DeleteCourseFromTeacherRequest){
        this.teacherService.deleteCourseFromTeacher(deleteCourseFromTeacherRequest);
    }



}