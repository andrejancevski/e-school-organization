package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CreateStudentGradeRequest(
        val courseId: Long,
        val studentId: String,
        val gradingPeriod: String,
        val examType: String,
        val grade: String,
        val points: Int,
        val gradeReview: String,
        val forGPA: Boolean
)