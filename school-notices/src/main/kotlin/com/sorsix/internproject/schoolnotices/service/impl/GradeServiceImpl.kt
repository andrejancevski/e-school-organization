package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Grade
import com.sorsix.internproject.schoolnotices.domain.Page
import com.sorsix.internproject.schoolnotices.domain.Student
import com.sorsix.internproject.schoolnotices.domain.modelDtos.GradeDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateStudentGradeRequest
import com.sorsix.internproject.schoolnotices.exceptions.StudentNotFoundException
import com.sorsix.internproject.schoolnotices.repository.*
import com.sorsix.internproject.schoolnotices.service.GradeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.PageRequest
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDate


@Service
class GradeServiceImpl(private val gradeRepository: GradeRepository,
                       private val studentRepository: StudentRepository,
                       private val courseRepository: CourseRepository,
                       private val userRepository: UserRepository) : GradeService {

    val logger: Logger = LoggerFactory.getLogger(GradeServiceImpl::class.java)

    override fun createNewGrade(createStudentGradeRequest: CreateStudentGradeRequest) {
        val teacherEmail = SecurityContextHolder.getContext().authentication.name;
        val student = this.studentRepository.findById(createStudentGradeRequest.studentId).orElseThrow { StudentNotFoundException("Student with this id is not found") }
        val course = this.courseRepository.findById(createStudentGradeRequest.courseId).orElseThrow()
        val user = this.userRepository.findByEmail(teacherEmail).orElseThrow()
        val newGPA = this.calculateGPA(student, createStudentGradeRequest.grade)
        student.gpa = newGPA

        this.gradeRepository.save(Grade(0, createStudentGradeRequest.grade,
                createStudentGradeRequest.gradingPeriod,
                createStudentGradeRequest.examType,
                LocalDate.now(),
                createStudentGradeRequest.points,
                createStudentGradeRequest.gradeReview,
                student = student, course = course, teacher = user.teacher, forGPA = createStudentGradeRequest.forGPA))

        logger.info("Grade [{}] added to student [{}]",createStudentGradeRequest.grade,student.name)
    }

    override fun getGradesForStudent(page: Int, size: Int, studentId: String): Page<GradeDTO>? {
        val role = SecurityContextHolder.getContext().authentication.authorities.toMutableList()
        val teacherEmail = SecurityContextHolder.getContext().authentication.name
        val roleString = role.get(0).authority
        if (roleString.equals("ROLE_TEACHER")) {
            val result = this.gradeRepository.findGradesByStudentIdAndTeacher(PageRequest.of(page, size), studentId,teacherEmail)
            return Page(page, result?.totalPages!!, size, result.content)
        } else {
            val result = this.gradeRepository.findGradesByStudentId(PageRequest.of(page, size), studentId)
            return Page(page, result?.totalPages!!, size, result.content)
        }
    }

    fun calculateGPA(student: Student, newGrade: String): Double {
        var countGrades = 0
        var sumOfGrades = 0;
        if (student.grades.isNullOrEmpty()) {
            countGrades = 0
            sumOfGrades = 0;
        } else {
            countGrades = student.grades.size
            sumOfGrades = student.grades.stream().mapToInt { it -> convertGradesToNum(it.name) }.sum();
        }
        return ((sumOfGrades + convertGradesToNum(newGrade)) / (countGrades + 1)).toDouble()

    }

    fun convertGradesToNum(grade: String): Int {
        return when (grade) {
            "A" -> 5
            "B" -> 4
            "C" -> 3
            "D" -> 2
            "E" -> 1
            else -> 0
        }
    }

}