package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CourseAddRequest(
        val courseId: Long,
        val courseName: String,
        val courseCredits: Int
)