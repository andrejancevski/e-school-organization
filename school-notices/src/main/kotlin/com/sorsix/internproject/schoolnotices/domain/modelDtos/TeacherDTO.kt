package com.sorsix.internproject.schoolnotices.domain.modelDtos


data class TeacherDTO(
        val teacherId: Long,
        val teacherCode: String,
        val teacherUserId: Long,
        val teacherFirstName: String,
        val teacherLastName: String,
        val teacherEmail: String
)