package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.Absence
import com.sorsix.internproject.schoolnotices.domain.modelDtos.AbsenceDTO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AbsenceRepository : JpaRepository<Absence,Long> {
    fun findAbsencesByParentAbsenceId(id:Long): List<Absence>?

    @Query("select new com.sorsix.internproject.schoolnotices.domain.Absence(a.id,a.reason,a.startDate,a.endDate,a.absentStudent,a.parentAbsence) from Absence a")
    fun getAll():List<Absence>?


}