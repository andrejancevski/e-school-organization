package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.Page
import com.sorsix.internproject.schoolnotices.domain.modelDtos.GradeDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateStudentGradeRequest
import com.sorsix.internproject.schoolnotices.service.GradeService
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/api/grade")
class GradeController(private val gradeService: GradeService) {

    @PostMapping("/newGrade")
    fun createGrade(@RequestBody newGradeRequest: CreateStudentGradeRequest) {
        this.gradeService.createNewGrade(newGradeRequest)
    }

    @GetMapping("/{studentId}")
    fun getGradesFromStudent(@RequestHeader(name = "page", defaultValue = "0", required = false) page: Int,
                             @RequestHeader(name = "page-size", defaultValue = "10", required = false) size: Int,
                             @PathVariable studentId: String): Page<GradeDTO>? {
        return this.gradeService.getGradesForStudent(page, size, studentId)
    }
}