package com.sorsix.internproject.schoolnotices

import com.sorsix.internproject.schoolnotices.domain.Course
import com.sorsix.internproject.schoolnotices.repository.CourseRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(name= arrayOf("app.db-init"), havingValue = true.toString())
class DataBaseInit(private val courseRepository: CourseRepository) : CommandLineRunner{
    override fun run(vararg args: String?) {

        if(courseRepository.count() == 0L){
            this.courseRepository.save(Course(0,"Biology",5))
            this.courseRepository.save(Course(0,"Chemistry",5))
            this.courseRepository.save(Course(0,"Algebra",5))
            this.courseRepository.save(Course(0,"Calculus",5))
            this.courseRepository.save(Course(0,"Advanced Mathematics",10))
            this.courseRepository.save(Course(0,"Cultural Science",5))
            this.courseRepository.save(Course(0,"History",5))
            this.courseRepository.save(Course(0,"Arts & Crafts",5))
            this.courseRepository.save(Course(0,"English Literature",10))
            this.courseRepository.save(Course(0,"Physical Education",5))
            this.courseRepository.save(Course(0,"Physics",10))
            this.courseRepository.save(Course(0,"Modern Physics",10))
            this.courseRepository.save(Course(0,"Introduction to French",10))
        }


    }

}