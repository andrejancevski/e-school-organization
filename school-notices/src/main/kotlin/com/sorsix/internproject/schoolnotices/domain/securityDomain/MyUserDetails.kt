package com.sorsix.internproject.schoolnotices.domain.securityDomain

import com.sorsix.internproject.schoolnotices.domain.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class MyUserDetails(user: User): UserDetails{

    private var userName: String = user.email
    private var password: String = user.password
    private var active: Boolean = user.active
    private var authorites: MutableCollection<GrantedAuthority> = user.roles.split(",")
            .map { it -> SimpleGrantedAuthority(it) }.toMutableList()




    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return this.authorites
    }

    override fun getPassword(): String {
        return this.password
    }

    override fun getUsername(): String {
        return this.userName
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return this.active
    }

}