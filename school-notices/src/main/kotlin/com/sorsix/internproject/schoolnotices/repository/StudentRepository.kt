package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.Student
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface StudentRepository: JpaRepository<Student,String>{

}