package com.sorsix.internproject.schoolnotices.domain.securityDomain

data class AuthenticationResponse(val jwt: String)