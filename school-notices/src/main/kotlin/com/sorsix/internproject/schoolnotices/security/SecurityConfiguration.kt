package com.sorsix.internproject.schoolnotices.security

import com.sorsix.internproject.schoolnotices.filters.JwtRequestFilter
import com.sorsix.internproject.schoolnotices.service.impl.MyUsersService
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfiguration(val myUsersService: MyUsersService, private val jwtRequestFilter: JwtRequestFilter) : WebSecurityConfigurerAdapter() {
    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(myUsersService)?.passwordEncoder(getPasswordEncoder())

    }

    override fun configure(web: WebSecurity?) {
        web?.ignoring()
                ?.antMatchers(HttpMethod.OPTIONS,"/**")
    }

    override fun configure(http: HttpSecurity?) {
        http?.cors()?.and()?.csrf()?.disable()
                ?.authorizeRequests()
                ?.antMatchers("/api/meeting/create")?.hasAnyRole("TEACHER")
                ?.antMatchers("/api/meeting/allMeetings")?.hasAnyRole("TEACHER","PARENT")
                ?.antMatchers("/api/notice/create")?.hasAnyRole("TEACHER","ADMIN")
                ?.antMatchers("/api/notice/**")?.authenticated()
                ?.antMatchers("/api/grade/newGrade")?.hasRole("TEACHER")
                ?.antMatchers("/api/grade/**")?.authenticated()
                ?.antMatchers("/api/absence/getAll")?.hasAnyRole("TEACHER","PARENT")
                ?.antMatchers("/api/absence/newAbsence")?.hasRole("PARENT")
                ?.antMatchers("/api/parents/parentStudents")?.hasRole("PARENT")
                ?.antMatchers("/api/course/allCourses")?.hasAnyRole("TEACHER","ADMIN")
                ?.antMatchers("/api/course/coursesTeachers")?.hasAnyRole("TEACHER","ADMIN")
                ?.antMatchers("/api/course/coursesTeacher")?.hasAnyRole("TEACHER", "ADMIN")
                ?.antMatchers("/api/course/students")?.hasAnyRole("TEACHER","ADMIN")
                ?.antMatchers("/api/course/student/**")?.hasAnyRole("TEACHER","ADMIN")
                ?.antMatchers("/api/parents/student/**")?.hasAnyRole("PARENT","TEACHER")
                ?.antMatchers("/api/parents/parentStudents")?.hasAnyRole("PARENT","TEACHER")
                ?.antMatchers("/api/student/create")?.hasRole("ADMIN")
                ?.antMatchers("/api/teacher/removeCourse")?.hasRole("ADMIN")
                ?.antMatchers("/api/teacher/addCourses")?.hasRole("ADMIN")
                ?.antMatchers("/authenticate")?.permitAll()
                ?.and()
                ?.sessionManagement()?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http?.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
    }


    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun getPasswordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder(10)
    }
}