package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name="absences")
data class Absence(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id : Long,
        val reason: String,
        val startDate: LocalDate,
        val endDate: LocalDate,
        val absentStudent: String,
        @ManyToOne
        @JsonIgnore
        val parentAbsence: Parent


)