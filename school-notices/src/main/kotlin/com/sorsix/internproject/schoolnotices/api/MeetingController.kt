package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.Meeting
import com.sorsix.internproject.schoolnotices.domain.modelDtos.MeetingDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateMeetingRequest
import com.sorsix.internproject.schoolnotices.service.MeetingService
import com.sorsix.internproject.schoolnotices.service.impl.MeetingServiceImpl
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/meeting")

class MeetingController(private val meetingService: MeetingService) {

    @PostMapping("/create")
    fun createMeeting(@RequestBody meetingRequest: CreateMeetingRequest) {
        meetingService.createNewMeeting(meetingRequest)
    }

    @GetMapping("allMeetings")
    fun getAllMeetings(): List<MeetingDTO>? {
        return this.meetingService.getAllMeetings();
    }

}