package com.sorsix.internproject.schoolnotices.domain.modelDtos

data class CourseTeacherDTO(
        val courseId: Long,
        val courseName: String,
        val courseCredits: Int,
        val teacherId: Long,
        val teacherCode: String,
        val teacherName: String,
        val teacherLastName: String

)