package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.securityDomain.MyUserDetails
import com.sorsix.internproject.schoolnotices.domain.User
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.*

@Service
class MyUsersService(val userRepository: UserRepository):UserDetailsService {
    override fun loadUserByUsername(email: String?): UserDetails {
       val user: Optional<User> = userRepository.findByEmail(email)
       user.orElseThrow { throw UsernameNotFoundException("NotFound" + email) }
       return user.map { MyUserDetails(user.get()) }.get()
    }

}