package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Parent
import com.sorsix.internproject.schoolnotices.domain.Student
import com.sorsix.internproject.schoolnotices.domain.Teacher
import com.sorsix.internproject.schoolnotices.domain.User
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateUserRequest
import com.sorsix.internproject.schoolnotices.repository.ParentRepository
import com.sorsix.internproject.schoolnotices.repository.StudentRepository
import com.sorsix.internproject.schoolnotices.repository.TeacherRepository
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import com.sorsix.internproject.schoolnotices.service.UserService
import javassist.NotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository,
                      private val teacherRepository: TeacherRepository,
                      private val studentRepository: StudentRepository,
                      private val parentRepository: ParentRepository,
                      private val passwordEncoder: PasswordEncoder) : UserService {

    val logger: Logger = LoggerFactory.getLogger(UserServiceImpl::class.java)

    override fun createNewUser(newUser: CreateUserRequest) {
        val user: User = User(0, newUser.firstName, newUser.lastName, newUser.email, passwordEncoder.encode(newUser.password), true, newUser.role)
        if (newUser.role.equals("ROLE_ADMIN")) {
            userRepository.save(user)
        } else if (newUser.role.equals("ROLE_TEACHER")) {
            this.teacherRepository.save(Teacher(0, newUser.teacherCode, user))
        } else if (newUser.role.equals("ROLE_PARENT")) {
            val students: MutableList<Student> = mutableListOf()
            val parent: Parent = Parent(0,user)
            parentRepository.save(parent)
            newUser.studentsEMBG.forEach { it ->
                run {
                    val student = studentRepository.findById(it).orElseThrow()
                    if (student.parents.isNullOrEmpty()){
                        student.parents = mutableListOf(parent)
                    }else{
                        student.parents!!.add(parent)
                    }
                    studentRepository.save(student)
                    students.add(student)
                }
            }
        }
        logger.info("User [{}] created",user)
    }

    override fun getUserById(id:String): User{
        return userRepository.findById(id.toLong()).orElseThrow { NotFoundException("User not found") }
    }

    override fun deleteUser(id: String) {
       userRepository.deleteById(id.toLong())
        logger.info("User with id [{}] deleted",id)
    }

    override fun updateUser(user: User) {  
        userRepository.save(this.getUserById(user.id.toString())
                .let {result->
                    User(result.id,
                    user.firstName,
                    user.lastName,
                    user.email,
                    user.password,
                    user.active,
                    user.roles,
                    result.notices,
                    result.parent,
                    result.teacher)
                })

    }
}