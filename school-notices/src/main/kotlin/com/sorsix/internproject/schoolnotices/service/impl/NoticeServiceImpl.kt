package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.ImageModel
import com.sorsix.internproject.schoolnotices.domain.Notice
import com.sorsix.internproject.schoolnotices.domain.Page
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDetailDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateNoticeRequest
import com.sorsix.internproject.schoolnotices.exceptions.NoticeNotFoundException
import com.sorsix.internproject.schoolnotices.repository.ImageRepository
import com.sorsix.internproject.schoolnotices.repository.NoticeRepository
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import com.sorsix.internproject.schoolnotices.service.NoticeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.PageRequest
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.time.LocalDate
import java.util.zip.DataFormatException
import java.util.zip.Deflater
import java.util.zip.Inflater

@Service
class NoticeServiceImpl(private val imageRepository: ImageRepository, private val noticeRepository: NoticeRepository, val userRepository: UserRepository) : NoticeService {

    val logger: Logger = LoggerFactory.getLogger(NoticeServiceImpl::class.java)

    override fun createNewNotice(file: MultipartFile, notice: CreateNoticeRequest) {
        val userName = SecurityContextHolder.getContext().authentication.name
        val imageModel = ImageModel(0, name = file.originalFilename!!, type = file.contentType!!,
                picByteArray = compressBytes(file.bytes)!!)
        val createdImage = this.imageRepository.save(imageModel)
        val user = this.userRepository.findByEmail(userName)
        this.noticeRepository.save(Notice(0, notice.noticeTitle, notice.noticeBody, notice.noticeImportance, LocalDate.now(),
                user.get(), createdImage))
        logger.info("Notice [{}] created",notice)
    }

    override fun getAllNotices(page: Int, size: Int): Page<NoticeDTO?> {
        val result = this.noticeRepository.findAllBy(PageRequest.of(page, size))
        result?.content?.forEach {it -> it?.imageModel?.picByteArray = decompressBytes(it?.imageModel?.picByteArray!!) }
        return Page(page, result?.totalPages!!, size, result.content)
    }

    override fun getFullNoticeById(id:String):NoticeDetailDTO{
        return this.noticeRepository.findById(id.toLong())
                .map {
                    result -> NoticeDetailDTO(
                        result.id,
                        result.title,
                        result.noticeText,
                        result.importanceLevel,
                        result.dateCreated,
                        ImageModel(result.imageModel.id,result.imageModel.name,result.imageModel.type,
                                this.decompressBytes(result.imageModel.picByteArray!!)),
                        result.user?.firstName+" "+result.user?.lastName) }
                .orElseThrow {NoticeNotFoundException("Notice with This ID does not exist") }
    }

    override fun deleteNotice(id: String) {
        this.noticeRepository.deleteById(id.toLong())
        logger.info("Notice with id [{}] deleted",id)
    }

    fun compressBytes(data: ByteArray): ByteArray? {
        val deflater = Deflater()
        deflater.setInput(data)
        deflater.finish()
        val outputStream = ByteArrayOutputStream(data.size)
        val buffer = ByteArray(1024)
        while (!deflater.finished()) {
            val count = deflater.deflate(buffer)
            outputStream.write(buffer, 0, count)
        }
        try {
            outputStream.close()
        } catch (e: IOException) {
        }
        return outputStream.toByteArray()
    }

    fun decompressBytes(data: ByteArray): ByteArray? {
        val inflater = Inflater()
        inflater.setInput(data)
        val outputStream = ByteArrayOutputStream(data.size)
        val buffer = ByteArray(1024)
        try {
            while (!inflater.finished()) {
                val count = inflater.inflate(buffer)
                outputStream.write(buffer, 0, count)
            }
            outputStream.close()
        } catch (ioe: IOException) {
        } catch (e: DataFormatException) {
        }
        return outputStream.toByteArray()
    }
}