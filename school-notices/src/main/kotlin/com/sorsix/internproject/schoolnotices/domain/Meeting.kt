package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table
data class Meeting(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val location: String,
        val reason: String,
        val dateTime: LocalDateTime,
        val dateCreated: LocalDate,
        @ManyToOne
        @JsonIgnore
        val teacherMeeting: Teacher,
        @ManyToOne
        @JsonIgnore
        val parent: Parent



)