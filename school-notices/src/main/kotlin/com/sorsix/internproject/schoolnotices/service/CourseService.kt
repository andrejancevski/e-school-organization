package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.Course
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseTeacherDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO


interface CourseService {
    fun getAllCourses(): List<CourseDTO>

    fun getAllCoursesWithTeachers():List<CourseTeacherDTO>

    fun getCoursesForTeacher(teacherEmail: String) : List<CourseDTO>?

    fun getAllStudentsFromCourse(courseId: Long) : MutableList<StudentDTO>?

    fun getCourseFromStudent(studentId: String): List<CourseDTO>?



}