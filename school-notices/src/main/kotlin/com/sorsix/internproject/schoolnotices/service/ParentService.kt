package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.modelDtos.ParentDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO

interface ParentService{
    fun getChildrenFromParents(): MutableList<StudentDTO>?

    fun getParentsFromStudent(studentId: String): List<ParentDTO>?
}