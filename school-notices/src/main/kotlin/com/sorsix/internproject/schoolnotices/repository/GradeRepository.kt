package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.Grade
import com.sorsix.internproject.schoolnotices.domain.modelDtos.GradeDTO
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface GradeRepository : JpaRepository<Grade, Long> {

    @Query("select new com.sorsix.internproject.schoolnotices.domain.modelDtos.GradeDTO(g.id,g.name,g.gradingPeriod," +
            "g.type,g.date,g.points,g.review,g.forGPA,g.course.name) from Grade g where g.student.EMBG = :studentId")
    fun findGradesByStudentId(pageable: Pageable, studentId: String): org.springframework.data.domain.Page<GradeDTO>?

    @Query("select new com.sorsix.internproject.schoolnotices.domain.modelDtos.GradeDTO(g.id,g.name,g.gradingPeriod," +
            "g.type,g.date,g.points,g.review,g.forGPA,g.course.name) from Grade g " +
            "where g.student.EMBG = :studentId and g.teacher.teacherUser.email = :teacherEmail")
    fun findGradesByStudentIdAndTeacher(pageable: Pageable, studentId: String, teacherEmail: String):
            org.springframework.data.domain.Page<GradeDTO>?
}