package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CreateNoticeRequest (val noticeTitle: String,
                                val noticeBody: String,
                                val noticeImportance: String)

