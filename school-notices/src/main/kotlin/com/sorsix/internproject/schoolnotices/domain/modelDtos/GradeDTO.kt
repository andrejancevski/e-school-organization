package com.sorsix.internproject.schoolnotices.domain.modelDtos

import java.time.LocalDate

data class GradeDTO(
        val id: Long,
        val name: String,
        val gradingPeriod: String,
        val type: String,
        val date: LocalDate,
        val points: Int,
        val review: String,
        val forGPA: Boolean,
        val courseName: String
)