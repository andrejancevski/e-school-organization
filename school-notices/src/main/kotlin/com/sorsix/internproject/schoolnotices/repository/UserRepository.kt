package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<User,Long>{

   fun findByEmail(email: String?): Optional<User>
}