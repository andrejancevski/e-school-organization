package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.User
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateUserRequest


interface UserService {
    fun createNewUser(newUser: CreateUserRequest)

    fun getUserById(id:String): User

    fun deleteUser(id:String)

    fun updateUser(user:User)
}