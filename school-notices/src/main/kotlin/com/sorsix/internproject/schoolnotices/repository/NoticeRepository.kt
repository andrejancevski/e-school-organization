package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.Notice
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDetailDTO
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface NoticeRepository : JpaRepository<Notice, Long> {

    @Query("select new com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDTO(n.id, n.title," +
            " n.noticeText,n.importanceLevel,n.dateCreated,n.imageModel) from Notice n order by n.dateCreated desc")
    fun findAllBy(pageable: Pageable): org.springframework.data.domain.Page<NoticeDTO?>?

}