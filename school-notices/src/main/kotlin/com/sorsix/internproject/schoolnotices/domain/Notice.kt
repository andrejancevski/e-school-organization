package com.sorsix.internproject.schoolnotices.domain

import java.time.LocalDate
import javax.persistence.*


@Entity
@Table(name="notices")
data class Notice(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val title: String,
        val noticeText: String,
        val importanceLevel: String,
        val dateCreated: LocalDate,
        @ManyToOne
        @JoinColumn(name="user_id")
        val user: User? = null,
        @OneToOne(cascade = arrayOf(CascadeType.ALL))
        @JoinColumn(name="image_id", referencedColumnName = "id")
        var imageModel: ImageModel

)