package com.sorsix.internproject.schoolnotices.repository

import com.sorsix.internproject.schoolnotices.domain.Course
import com.sorsix.internproject.schoolnotices.domain.Teacher
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseTeacherDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.TeacherDTO

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CourseRepository : JpaRepository<Course, Long> {

    @Query("select new com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseDTO(c.id,c.name,c.credits) from Course c")
    fun findAllBy(): List<CourseDTO>

    @Query("select new com.sorsix.internproject.schoolnotices.domain.modelDtos.CourseTeacherDTO(c.id,c.name,c.credits,t.id,t.code,t.teacherUser.firstName,t.teacherUser.lastName) from Course c join c.teachersByCourse t")
    fun getAllCoursesWithTeachers(): List<CourseTeacherDTO>

}