package com.sorsix.internproject.schoolnotices.domain.modelDtos

import java.time.LocalDate
import java.time.LocalDateTime

data class MeetingDTO(
        val meetingId: Long,
        val location: String,
        val reason: String,
        val dateTime: LocalDateTime,
        val dateCreated: LocalDate,
        val teacherOfTheMeeting: String,
        val parentOfTheMeeting: String
)