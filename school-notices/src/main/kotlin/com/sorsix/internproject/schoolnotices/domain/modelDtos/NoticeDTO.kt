package com.sorsix.internproject.schoolnotices.domain.modelDtos

import com.sorsix.internproject.schoolnotices.domain.ImageModel
import java.time.LocalDate

data class NoticeDTO(val id: Long,
                     val title:String,
                     val noticeText: String,
                     val importanceLevel: String,
                     val dateCreated: LocalDate,
                     val imageModel: ImageModel){
}