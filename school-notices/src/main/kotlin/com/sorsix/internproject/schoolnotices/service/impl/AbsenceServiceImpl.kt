package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Absence
import com.sorsix.internproject.schoolnotices.domain.modelDtos.AbsenceDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateAbsenceRequest
import com.sorsix.internproject.schoolnotices.exceptions.AbsencesNotFoundException
import com.sorsix.internproject.schoolnotices.exceptions.UserNotFoundException
import com.sorsix.internproject.schoolnotices.repository.AbsenceRepository
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import com.sorsix.internproject.schoolnotices.service.AbsenceService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Service
class AbsenceServiceImpl(private val absenceRepository: AbsenceRepository,
                         val userRepository: UserRepository) : AbsenceService{

    val formatter = DateTimeFormatter.ofPattern("d/MM/yyyy")

    val logger: Logger = LoggerFactory.getLogger(AbsenceServiceImpl::class.java)

    override fun createNewAbsence(newAbsenceRequest: CreateAbsenceRequest) {
        val userName = SecurityContextHolder.getContext().authentication.name
        val user = userRepository.findByEmail(userName).orElseThrow{UserNotFoundException("This user is not in the system")}
        val absence = Absence(0,newAbsenceRequest.absenceReason,
                LocalDate.parse(newAbsenceRequest.startDate,formatter),
                LocalDate.parse(newAbsenceRequest.endDate,formatter),
                newAbsenceRequest.studentName, user.parent!!)
        absenceRepository.save(absence)
        logger.info("Absence [{}] created",absence)
    }

    override fun getAbsences():List<AbsenceDTO>{
        val user=userRepository.findByEmail(SecurityContextHolder.getContext().authentication.name).orElseThrow()
        if (user.roles=="ROLE_PARENT"){
             return absenceRepository.findAbsencesByParentAbsenceId(user.parent?.id?:0)
                     ?.map{absence -> AbsenceDTO(absence.id,
                             absence.reason,
                             absence.startDate.toString(),
                             absence.endDate.toString(),
                             absence.absentStudent,
                             absence.parentAbsence.parentUser.firstName + " "+ absence.parentAbsence.parentUser.lastName)}
                     ?:throw(AbsencesNotFoundException("No absences found for this user"))
         }
        else {
             return absenceRepository.getAll()
                     ?.map{absence -> AbsenceDTO(absence.id,
                                                absence.reason,
                                                absence.startDate.toString(),
                                                absence.endDate.toString(),
                                                absence.absentStudent,
                                                absence.parentAbsence.parentUser.firstName + " "+ absence.parentAbsence.parentUser.lastName)}
                     ?:throw(AbsencesNotFoundException("No absences found"))
         }
    }
}