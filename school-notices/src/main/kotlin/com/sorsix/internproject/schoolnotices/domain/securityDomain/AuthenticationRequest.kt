package com.sorsix.internproject.schoolnotices.domain.securityDomain

data class AuthenticationRequest(val userName: String, val password: String){
}