package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.securityDomain.AuthenticationRequest
import com.sorsix.internproject.schoolnotices.domain.securityDomain.AuthenticationResponse
import com.sorsix.internproject.schoolnotices.service.impl.MyUsersService
import com.sorsix.internproject.schoolnotices.util.JwtUtil
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
class AuthenticationController(private val myUsersService: MyUsersService,
                               private val jwtTokenUtil: JwtUtil,
                               private val authenticationManager: AuthenticationManager){


    @PostMapping("/authenticate")
    fun createAuthenticationToken(@RequestBody authenticationRequest: AuthenticationRequest): ResponseEntity<Any>{

        try{
            this.authenticationManager.authenticate(
                    UsernamePasswordAuthenticationToken(authenticationRequest.userName,authenticationRequest.password)
            )
        }catch (e: BadCredentialsException){
            throw Exception("Incorrect username or password",e)
        }

        val userDetails: UserDetails = this.myUsersService.loadUserByUsername(authenticationRequest.userName)
        val jwt: String = this.jwtTokenUtil.generateToken(userDetails)
        return ResponseEntity.ok(AuthenticationResponse(jwt))

    }
}