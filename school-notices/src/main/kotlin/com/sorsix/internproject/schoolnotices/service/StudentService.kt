package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDetailsDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.NewStudentRequest


interface StudentService {
    fun createNewUser(newStudent: NewStudentRequest) {}

     fun getStudentById(id:String): StudentDetailsDTO

    fun deleteStudent(id:String)

}