package com.sorsix.internproject.schoolnotices.service

import com.sorsix.internproject.schoolnotices.domain.Meeting
import com.sorsix.internproject.schoolnotices.domain.modelDtos.MeetingDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateMeetingRequest

interface MeetingService {

    fun createNewMeeting(meeting: CreateMeetingRequest)

    fun getAllMeetings(): List<MeetingDTO>?
}