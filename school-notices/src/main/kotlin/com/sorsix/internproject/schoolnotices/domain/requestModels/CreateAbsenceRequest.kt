package com.sorsix.internproject.schoolnotices.domain.requestModels

data class CreateAbsenceRequest(
        val studentName: String,
        val startDate: String,
        val endDate: String,
        val absenceReason: String
)