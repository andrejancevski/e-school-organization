package com.sorsix.internproject.schoolnotices.service.impl

import com.sorsix.internproject.schoolnotices.domain.Student
import com.sorsix.internproject.schoolnotices.domain.modelDtos.ParentDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.StudentDTO
import com.sorsix.internproject.schoolnotices.exceptions.ParentsNotRegisteredException
import com.sorsix.internproject.schoolnotices.exceptions.StudentNotFoundException
import com.sorsix.internproject.schoolnotices.exceptions.UserNotFoundException
import com.sorsix.internproject.schoolnotices.repository.StudentRepository
import com.sorsix.internproject.schoolnotices.repository.UserRepository
import com.sorsix.internproject.schoolnotices.service.ParentService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class ParentServiceImpl(private val userRepository: UserRepository,
                        private val studentRepository: StudentRepository) : ParentService{
    override fun getChildrenFromParents(): MutableList<StudentDTO>? {
        val userName = SecurityContextHolder.getContext().authentication.name
        val parent = userRepository.findByEmail(userName).orElseThrow{UserNotFoundException("User with this id does not exist")}
        return parent.parent?.students?.map { it-> StudentDTO(it.EMBG,it.name) }?.toMutableList()
    }

    override fun getParentsFromStudent(studentId: String): List<ParentDTO>? {
        val student = this.studentRepository.findById(studentId).orElseThrow{ StudentNotFoundException("Student with this id does not exist")}
        if(student.parents.isNullOrEmpty()){
            throw ParentsNotRegisteredException("Parent's not registered yet, try later")
        }else{
            return student.parents!!.map { it -> ParentDTO(it.id,it.parentUser.firstName,it.parentUser.lastName) }.toList();
        }
    }

}