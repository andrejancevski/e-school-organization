package com.sorsix.internproject.schoolnotices.api


import com.sorsix.internproject.schoolnotices.domain.Page
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDTO
import com.sorsix.internproject.schoolnotices.domain.modelDtos.NoticeDetailDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateNoticeRequest
import com.sorsix.internproject.schoolnotices.service.NoticeService

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/notice")
class NoticeController(private val noticeService: NoticeService) {

    @PostMapping(value = ["/create"], consumes = arrayOf(MediaType.MULTIPART_FORM_DATA_VALUE))
    fun createNotice(@RequestPart(value = "file", required = true) file: MultipartFile,
                     @RequestPart(value = "notice", required = true) notice: CreateNoticeRequest) {
        this.noticeService.createNewNotice(file, notice)
    }

    @GetMapping(path = arrayOf("/allNotices"))
    fun getAllNotices(@RequestHeader(name = "page", defaultValue = "0", required = false) page: Int,
                      @RequestHeader(name = "page-size", defaultValue = "10", required = false) size: Int): Page<NoticeDTO?> {
        return noticeService.getAllNotices(page, size)
    }

    @GetMapping("/{id}")
    fun getNoticeById(@PathVariable id: String): NoticeDetailDTO {
        return noticeService.getFullNoticeById(id)
    }

    @DeleteMapping("/{id}")
    fun deleteNotice(@PathVariable id: String) {
        println("Delete" + id)
        noticeService.deleteNotice(id)
    }
}