package com.sorsix.internproject.schoolnotices.domain.modelDtos

import com.sorsix.internproject.schoolnotices.domain.ImageModel
import java.time.LocalDate

data class NoticeDetailDTO(
        val id: Long,
        val title: String,
        val noticeText: String,
        val importanceLevel: String,
        val dateCreated: LocalDate,
        val imageModel: ImageModel,
        val user: String)