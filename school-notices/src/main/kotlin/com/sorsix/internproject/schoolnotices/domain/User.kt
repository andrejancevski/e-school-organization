package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name="users")
data class User(
        @Id
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        val id: Long,
        val firstName: String,
        val lastName: String,
        val email: String,
        val password: String,
        val active: Boolean,
        val roles: String,
        @OneToMany(mappedBy = "user")
        @JsonIgnore
        val notices: List<Notice>? = null,
        @OneToOne(mappedBy = "parentUser")
        @JsonIgnore
        val parent: Parent? = null,
        @OneToOne(mappedBy = "teacherUser")
        @JsonIgnore
        val teacher: Teacher? = null

)