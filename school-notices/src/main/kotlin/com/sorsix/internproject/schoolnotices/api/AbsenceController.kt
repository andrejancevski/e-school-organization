package com.sorsix.internproject.schoolnotices.api

import com.sorsix.internproject.schoolnotices.domain.modelDtos.AbsenceDTO
import com.sorsix.internproject.schoolnotices.domain.requestModels.CreateAbsenceRequest
import com.sorsix.internproject.schoolnotices.service.AbsenceService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping("/api/absence")
class AbsenceController(private val absenceService: AbsenceService) {

    @PostMapping("/newAbsence")
    fun createAbsence(@RequestBody newAbsenceRequest: CreateAbsenceRequest) {
        this.absenceService.createNewAbsence(newAbsenceRequest)
    }

    @GetMapping("/getAll")
    fun getAbsences():List<AbsenceDTO>{
       return this.absenceService.getAbsences()


    }

}