package com.sorsix.internproject.schoolnotices.domain.modelDtos

data class ParentDTO(
        val parentId: Long,
        val parentFirstName: String,
        val parentLastName: String
)