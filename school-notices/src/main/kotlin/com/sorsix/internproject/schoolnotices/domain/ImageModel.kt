package com.sorsix.internproject.schoolnotices.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name="image_table")
data class ImageModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        var name: String,
        val type: String,
        var picByteArray: ByteArray?,
        @OneToOne(mappedBy = "imageModel")
        @JsonIgnore
        val notice: Notice? = null

)
