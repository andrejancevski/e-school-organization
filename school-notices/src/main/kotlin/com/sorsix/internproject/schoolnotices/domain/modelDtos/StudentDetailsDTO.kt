package com.sorsix.internproject.schoolnotices.domain.modelDtos

import java.time.LocalDate

data class StudentDetailsDTO(
        val studentEMBG: String,
        val studentName: String,
        val studentDOB: LocalDate,
        val schoolYear: String,
        val gpa: Double,
        val dateEnrolled: LocalDate,
        val teachers: MutableList<String>?,
        val courses: MutableList<String>?,
        val parents: MutableList<String>?
)