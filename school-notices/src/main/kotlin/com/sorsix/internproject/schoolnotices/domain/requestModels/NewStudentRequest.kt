package com.sorsix.internproject.schoolnotices.domain.requestModels

data class NewStudentRequest(
        val studentName: String,
        val studentEMBG: String,
        val studentDOB: String,
        val studentSchoolYear: String,
        val coursesTeachers: MutableList<CoursesTeachersRequest>
)