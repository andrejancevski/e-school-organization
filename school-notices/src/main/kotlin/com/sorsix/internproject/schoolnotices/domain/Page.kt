package com.sorsix.internproject.schoolnotices.domain

import com.sorsix.internproject.schoolnotices.exceptions.InvalidPageException



data class Page<T>(val page: Int,val totalPages: Int, val pageSize: Int, val content: List<T>) {
//    private val page = 0
//    private val totalPages = 0
//    private val pageSize = 0
//    private val content: List<T>? = null



    companion object {
        fun <T> slice(content: List<T>, page: Int, pageSize: Int): Page<T> {
            val pageStart = page * pageSize
            var pageEnd = (page + 1) * pageSize
            if (pageStart > content.size || pageStart < 0 || pageEnd < 0) {
                throw InvalidPageException()
            }
            if (pageEnd > content.size) {
                pageEnd = content.size
            }
            return Page(page,
                    Math.ceil(1.0 * content.size / pageSize).toInt(),
                    pageSize,
                    content.subList(pageStart, pageEnd))
        }
    }
}
