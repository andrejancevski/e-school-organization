import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../authentication/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-first-login',
  templateUrl: './first-login.component.html',
  styleUrls: ['./first-login.component.css']
})
export class FirstLoginComponent implements OnInit, OnDestroy {

  @ViewChild('formElement') firstLogIn: NgForm;
  errorMessage = false;
  subscription: Subscription;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.subscription = this.authService.errorThrown.subscribe(it => {
      this.errorMessage = it;
    });
  }

  onSubmit(firstLogIn: NgForm): void {
    this.authService.logInUser(firstLogIn.value.email, firstLogIn.value.password);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
