import {Component, Input, OnInit} from '@angular/core';
import {Meeting} from '../../models/Meeting.model';

@Component({
  selector: 'app-meeting-slot',
  templateUrl: './meeting-slot.component.html',
  styleUrls: ['./meeting-slot.component.css', '../../app.component.css'],
})
export class MeetingSlotComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('meeting') meeting: Meeting;

  constructor() {
  }

  ngOnInit(): void {
  }
}
