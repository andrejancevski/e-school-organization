import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {Meeting} from '../models/Meeting.model';
import {MeetingsService} from '../services/meetings.service';

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.css', '../app.component.css'],
})
export class MeetingsComponent implements OnInit {
  roleState = '';
  meetings$: Observable<Meeting[]>;

  constructor(private route: ActivatedRoute, private router: Router, private meetingService: MeetingsService) {
  }

  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');
    this.meetings$ = this.meetingService.getAllMeetings();
  }

  onNewMeetCreate(): void {
    this.router.navigate(['newMeeting'], {relativeTo: this.route.parent});
  }
}
