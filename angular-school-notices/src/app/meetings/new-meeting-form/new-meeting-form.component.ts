import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ShortParent} from '../../models/ShortParent.model';
import {ParentsService} from '../../services/parents.service';
import {min} from 'rxjs/operators';
import {MeetingsService} from '../../services/meetings.service';

@Component({
  selector: 'app-new-meeting-form',
  templateUrl: './new-meeting-form.component.html',
  styleUrls: ['./new-meeting-form.component.css', '../../app.component.css'],
})
export class NewMeetingFormComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router,
              private parentsService: ParentsService,
              private meetingService: MeetingsService) {
  }

  date: NgbDateStruct;
  parents$: Observable<ShortParent[]>;
  @ViewChild('formElement') createMeetingForm: NgForm;

  time: NgbTimeStruct;

  ctrl = new FormControl('', (control: FormControl) => {
    const value = control.value;

    if (!value) {
      return {noValue: true};
    }

    if (value.hour < 7 || value.hour > 19) {
      return {invalidHour: true};
    }

    return null;
  });

  ngOnInit(): void {
  }

  formatDate(date: any): string {
    let month = date.month;
    let day = date.day;
    if (+date.day < 10) {
      day = '0' + date.day;
    }
    if (+date.month < 10) {
      month = '0' + date.month;
    }

    return day + '/' + month + '/' + date.year;
  }

  formatTime(time: any): string {
    let hour = time.hour;
    let minute = time.minute;
    if (hour < 10) {
      hour = '0' + hour;
    }
    if (minute < 10) {
      minute = '0' + minute;
    }
    return hour + ':' + minute;
  }

  onSubmit(): void {
    const newMeeting = {
      studentId: this.createMeetingForm.value.studentEMBG,
      reasonForMeeting: this.createMeetingForm.value.subject,
      location: this.createMeetingForm.value.location,
      dateTime: this.formatDate(this.date) + ' ' + this.formatTime(this.time)
    };
    this.meetingService.createNewMeeting(newMeeting);
    this.router.navigate(['meetings'], {relativeTo: this.route.parent});
  }

  onTypingFinish($event): void {
    this.parents$ = this.parentsService.getParentsFromStudent($event.target.value);
  }
}
