import {Component, OnInit} from '@angular/core';
import {OpenModalService} from '../../modals/open-modal.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-main-display',
  templateUrl: './main-display.component.html',
  styleUrls: ['./main-display.component.css']
})
export class MainDisplayComponent implements OnInit {

  constructor(private omService: OpenModalService) {
  }

  ngOnInit(): void {
  }

  onParentRegister(): void {
    this.omService.openParentModal();
  }

  onAdminRegister(): void {
    this.omService.openAdminModal();
  }

  onTeacherRegister(): void {
    this.omService.openTeacherModal();
  }
}
