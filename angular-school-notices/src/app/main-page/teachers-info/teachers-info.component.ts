import {Component, OnInit} from '@angular/core';
import {OpenModalService} from '../../modals/open-modal.service';

@Component({
  selector: 'app-teachers-info',
  templateUrl: './teachers-info.component.html',
  styleUrls: ['./teachers-info.component.css']
})
export class TeachersInfoComponent implements OnInit {

  constructor(private omService: OpenModalService) {
  }

  ngOnInit(): void {
  }

  onTeacherJoin(): void {
    this.omService.openTeacherModal();
  }
}
