import {Component, OnInit} from '@angular/core';
import {OpenModalService} from '../../modals/open-modal.service';

@Component({
  selector: 'app-parents-info',
  templateUrl: './parents-info.component.html',
  styleUrls: ['./parents-info.component.css']
})
export class ParentsInfoComponent implements OnInit {

  constructor(private omService: OpenModalService) {
  }

  ngOnInit(): void {
  }

  onParentsJoin(): void {
    this.omService.openParentModal();
  }
}
