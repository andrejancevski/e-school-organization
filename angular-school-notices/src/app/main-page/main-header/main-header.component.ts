import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {OpenModalService} from '../../modals/open-modal.service';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {


  // @Output() scroll: EventEmitter<any> = new EventEmitter();
  constructor(private omService: OpenModalService) {
  }

  ngOnInit(): void {
  }

  logIn(): void {
    this.omService.openLogInModal();
  }

  // scrollToParent(){
  //   this.scroll.emit("parent");
  // }

  // scrollToTeacher(){
  //   this.scroll.emit(true);
  // }
}
