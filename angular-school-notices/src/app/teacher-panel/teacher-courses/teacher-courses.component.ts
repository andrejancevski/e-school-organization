import {Component, OnInit} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Observable} from 'rxjs';
import {Course} from '../../models/Course.model';
import {CoursesService} from '../../services/courses.service';

@Component({
  selector: 'app-teacher-courses',
  templateUrl: './teacher-courses.component.html',
  styleUrls: ['./teacher-courses.component.css']
})
export class TeacherCoursesComponent implements OnInit {

  helper = new JwtHelperService();
  courses$: Observable<Course[]>;

  constructor(private coursesService: CoursesService) {
  }

  ngOnInit(): void {
    const decodedToken = this.helper.decodeToken(localStorage.getItem('token'));
    if (decodedToken.roles[0] === 'ROLE_TEACHER') {
      this.courses$ = this.coursesService.getAllCoursesFromTeacher(decodedToken.sub);
    } else {
      this.courses$ = this.coursesService.getAllCourses();
    }
  }

}
