import {Component, Input, OnInit} from '@angular/core';
import {StudentService} from '../../../services/student.service';

@Component({
  selector: 'app-teacher-course',
  templateUrl: './teacher-course.component.html',
  styleUrls: ['./teacher-course.component.css', '../../../app.component.css']
})
export class TeacherCourseComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('courseName') courseName: string;
  // tslint:disable-next-line:no-input-rename
  @Input('courseId') courseId: number;
  collapseName = '#collapseExample1';

  constructor(private studentService: StudentService) {
  }

  ngOnInit(): void {
  }

  onCourseSelected(courseId: number): void {
    this.studentService.getStudentsFromCourse(courseId);
  }
}
