import {NoticesService} from '../../services/notices.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Notice} from 'src/app/models/Notice.model';
import {Subscription} from 'rxjs';
import {NoticeDisplay} from '../../models/NoticeDisplay.model';

@Component({
  selector: 'app-notice-detail',
  templateUrl: './notice-detail.component.html',
  styleUrls: ['./notice-detail.component.css'],
})
export class NoticeDetailComponent implements OnInit, OnDestroy {
  notice: NoticeDisplay;
  subscription: Subscription;
  image: string;

  constructor(private activatedRoute: ActivatedRoute, private noticeService: NoticesService) {
  }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.noticeService.getNoticeById(id);
    this.subscription = this.noticeService.noticeDetail.subscribe((it) => {
      this.notice = it;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
