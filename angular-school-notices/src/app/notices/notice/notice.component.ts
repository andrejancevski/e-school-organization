import {NoticesService} from './../../services/notices.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Component, Input, OnInit} from '@angular/core';
import {NoticeDisplay} from '../../models/NoticeDisplay.model';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.css', '../../app.component.css'],
})
export class NoticeComponent implements OnInit {
  roleState = '';
  // tslint:disable-next-line:no-input-rename
  @Input('notice') notice: NoticeDisplay;


  constructor(private route: ActivatedRoute, private router: Router, private noticesService: NoticesService) {
  }

  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');

  }

  viewMoreDetails(noticeId: number): void {
    this.router.navigate(['notice', noticeId], {relativeTo: this.route.parent});
  }

  deleteNotice(noticeId: number): void {
    this.noticesService.deleteNoticeById(noticeId);
    window.location.reload();
  }
}
