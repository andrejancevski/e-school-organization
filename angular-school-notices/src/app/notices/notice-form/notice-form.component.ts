import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {NoticesService} from '../../services/notices.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notice-form',
  templateUrl: './notice-form.component.html',
  styleUrls: ['./notice-form.component.css', '../../app.component.css']
})
export class NoticeFormComponent implements OnInit {

  @ViewChild('uploadedPhoto') uploaded;
  @ViewChild('formElement') createNoticeForm: NgForm;
  fileToSend = null;
  image: any;

  constructor(private noticeService: NoticesService, private router: Router) {
  }

  ngOnInit(): void {
  }

  getCurrentRole(): string {
    const role = localStorage.getItem('role');
    if (role === 'ROLE_TEACHER') {
      return 'teacher';
    } else if (role === 'ROLE_ADMIN') {
      return 'admin';
    }
  }


  onSubmit(): void {

    const formData = new FormData();
    formData.append('file', this.fileToSend);
    formData.append('notice', new Blob([JSON.stringify({
        noticeTitle: this.createNoticeForm.value.noticeTitle,
        noticeBody: this.createNoticeForm.value.noticeBody,
        noticeImportance: this.createNoticeForm.value.importanceLevel
      })],
      {
        type: 'application/json'
      }));
    this.noticeService.createNewNotice(formData);
    this.router.navigate([this.getCurrentRole(), 'notices']);
  }


  handleFileInput(event: any): void {
    this.fileToSend = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (it) => {
      this.image = reader.result;
    };
  }
}
