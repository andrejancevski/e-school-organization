import {Component, OnDestroy, OnInit, ChangeDetectorRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NoticesService} from '../services/notices.service';
import {PageNoticeDisplay} from '../models/PageNoticeDisplay.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-notices',
  templateUrl: './notices.component.html',
  styleUrls: ['./notices.component.css']
})
export class NoticesComponent implements OnInit, OnDestroy {
  roleState = '';
  backgroundColor = 'white';
  collectionSize = 10;
  page = 1;
  pageNoticeDisplay: PageNoticeDisplay;
  subscription: Subscription;

  constructor(private route: ActivatedRoute, private router: Router,
              private noticeService: NoticesService, private changeDetection: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');
    this.noticeService.getAllNotices(this.page, 5);
    this.subscription = this.noticeService.pageNoticeLoaded.subscribe(it => {
      this.pageNoticeDisplay = it;
      this.changeDetection.detectChanges();
      this.collectionSize = it.totalPages * 10;
    });
  }

  newNotice(): void {
    this.router.navigate(['newNotice'], {relativeTo: this.route.parent});
  }

  onPageChange($event: number): void {
    this.noticeService.getAllNotices($event, 5);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
