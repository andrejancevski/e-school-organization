import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {StudentService} from '../../services/student.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {NgForm} from '@angular/forms';
import {AbsenceService} from '../../services/absence.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-absence-form',
  templateUrl: './new-absence-form.component.html',
  styleUrls: ['./new-absence-form.component.css', '../../app.component.css']
})
export class NewAbsenceFormComponent implements OnInit {

  modelStart: NgbDateStruct;
  modelEnd: NgbDateStruct;
  students$: Observable<any>;
  helper = new JwtHelperService();
  userEmail: string;
  @ViewChild('formElement') createAbsenceForm: NgForm;


  constructor(private studentService: StudentService,
              private absenceService: AbsenceService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    const decodedToken = this.helper.decodeToken(localStorage.getItem('token'));
    this.userEmail = decodedToken.sub;
    this.students$ = this.studentService.getStudentFromParent();
  }

  formatDate(dateOfBirth): string {
    let month = dateOfBirth.month;
    let day = dateOfBirth.day;
    if (+dateOfBirth.day < 10) {
      day = '0' + dateOfBirth.day;
    }
    if (+dateOfBirth.month < 10) {
      month = '0' + dateOfBirth.month;
    }

    return day + '/' + month + '/' + dateOfBirth.year;
  }

  onSubmit(): void {
    const newAbsence = {
      studentName: this.createAbsenceForm.value.studentName,
      startDate: this.formatDate(this.createAbsenceForm.value.start),
      endDate: this.formatDate(this.createAbsenceForm.value.end),
      absenceReason: this.createAbsenceForm.value.absence,
    };
    this.absenceService.createNewAbsence(newAbsence);
    this.router.navigate(['absences'], {relativeTo: this.route.parent});
  }
}
