import {Component, OnInit, Input} from '@angular/core';
import {Absence} from '../../models/Absence.model';

@Component({
  selector: 'app-abscence-slot',
  templateUrl: './absence-slot.component.html',
  styleUrls: ['./absence-slot.component.css']
})
export class AbsenceSlotComponent implements OnInit {
  @Input() absence: Absence;

  constructor() {
  }

  ngOnInit(): void {

  }

}
