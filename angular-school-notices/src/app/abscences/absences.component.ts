import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Absence} from '../models/Absence.model';
import {AbsenceService} from '../services/absence.service';

@Component({
  selector: 'app-abscences',
  templateUrl: './absences.component.html',
  styleUrls: ['./absences.component.css', '../app.component.css']
})
export class AbsencesComponent implements OnInit {
  roleState = '';

  message = '';

  absences: Absence[];

  constructor(private route: ActivatedRoute, private router: Router, private absenceService: AbsenceService) {
  }


  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');
    this.absenceService.getAbsences();
    this.absenceService.absences.subscribe(it => {
      this.absences = it;
    });
  }

  onNewAbsenceCreate(): void {
    this.router.navigate(['newAbsence'], {relativeTo: this.route.parent});
  }

  noAbsenceMessage(): void {
    if (this.roleState === 'ROLE_PARENT') {
      this.message = 'Your student doesn\'t have any absences';
    } else {
      this.message = 'No student has any absences';
    }
  }


}
