import {NoticeDetailComponent} from './notices/notice-detail/notice-detail.component';
import {NewAbsenceFormComponent} from './abscences/new-absence-form/new-absence-form.component';
import {ParentPanelComponent} from './parent-panel/parent-panel.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainPageComponent} from './main-page/main-page.component';
import {TeacherPanelComponent} from './teacher-panel/teacher-panel.component';
import {NoticesComponent} from './notices/notices.component';
import {MeetingsComponent} from './meetings/meetings.component';
import {NewMeetingFormComponent} from './meetings/new-meeting-form/new-meeting-form.component';
import {AbsencesComponent} from './abscences/absences.component';
import {NoticeFormComponent} from './notices/notice-form/notice-form.component';
import {StudentGroupsComponent} from './student-groups/student-groups.component';
import {TeacherCoursesComponent} from './teacher-panel/teacher-courses/teacher-courses.component';
import {StudentDetailComponent} from './student-detail/student-detail.component';
import {StudentGradesComponent} from './student-detail/student-grades/student-grades.component';
import {NewGradeFormComponent} from './student-detail/new-grade-form/new-grade-form.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {StudentsComponent} from './student-groups/students/students.component';
import {NewStudentComponent} from './new-student/new-student.component';
import {AddTeacherCoursesComponent} from './add-teacher-courses/add-teacher-courses.component';
import {FirstLoginComponent} from './first-login/first-login.component';
import {AuthParentGuardService} from './authentication/auth-parent-guard.service';
import {AuthAdminGuardService} from './authentication/auth-admin-guard.service';
import {AuthTeacherGuardService} from './authentication/auth-teacher-guard.service';


const routes: Routes = [
  {path: 'teacher', redirectTo: 'teacher/notices', pathMatch: 'full'},
  {path: 'parent', redirectTo: 'parent/notices', pathMatch: 'full'},
  {path: 'admin', redirectTo: 'admin/notices', pathMatch: 'full'},
  {path: '', component: MainPageComponent},
  {path: 'firstLogIn', component: FirstLoginComponent},
  {
    path: 'teacher', component: TeacherPanelComponent,
    canActivate: [AuthTeacherGuardService],
    children: [
      {path: 'notices', component: NoticesComponent},
      {path: 'notice/:id', component: NoticeDetailComponent},
      {path: 'newNotice', component: NoticeFormComponent},
      {path: 'meetings', component: MeetingsComponent},
      {path: 'newMeeting', component: NewMeetingFormComponent},
      {path: 'absences', component: AbsencesComponent},
      {path: 'newAbsence', component: NewAbsenceFormComponent},
      {path: 'students', component: StudentGroupsComponent},
      {path: 'courses', component: TeacherCoursesComponent},
      {path: 'student/:id', component: StudentDetailComponent},
      {path: 'student/:id/grade', component: StudentGradesComponent},
      {path: 'student/:id/newGrade', component: NewGradeFormComponent}
    ]
  },
  {
    path: 'parent', component: ParentPanelComponent,
    canActivate: [AuthParentGuardService],
    children: [
      {path: 'notices', component: NoticesComponent},
      {path: 'notice/:id', component: NoticeDetailComponent},
      {path: 'meetings', component: MeetingsComponent},
      {path: 'newMeeting', component: NewMeetingFormComponent},
      {path: 'newAbsence', component: NewAbsenceFormComponent},
      {path: 'absences', component: AbsencesComponent},
      {path: 'student/:id', component: StudentDetailComponent},
      {path: 'student/:id/grade', component: StudentGradesComponent}
    ]
  },
  {
    path: 'admin', component: AdminPanelComponent,
    canActivate: [AuthAdminGuardService],
    children: [
      {path: 'notices', component: NoticesComponent},
      {path: 'notice/:id', component: NoticeDetailComponent},
      {path: 'newNotice', component: NoticeFormComponent},
      {path: 'allStudents', component: StudentsComponent},
      {path: 'studentsByGroup', component: StudentGroupsComponent},
      {path: 'studentsByCourse', component: TeacherCoursesComponent},
      {path: 'newStudent', component: NewStudentComponent},
      {path: 'courses', component: AddTeacherCoursesComponent},
      {path: 'student/:id', component: StudentDetailComponent},
      {path: 'student/:id/grade', component: StudentGradesComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
