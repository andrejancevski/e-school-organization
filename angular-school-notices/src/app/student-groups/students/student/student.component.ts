import {Component, Input, OnInit} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('studentName') studentName;
  // tslint:disable-next-line:no-input-rename
  @Input('studentId') studentId;
  currentRole: string;

  constructor() {
  }

  setRoleForLink(): string {
    const currentRole = localStorage.getItem('role');
    if (currentRole === 'ROLE_TEACHER') {
      return 'teacher';
    } else {
      return 'admin';
    }

  }

  ngOnInit(): void {
    this.currentRole = this.setRoleForLink();
  }

}
