import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ShortStudent} from '../../models/ShortStudent.model';
import {StudentService} from '../../services/student.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students: ShortStudent[];

  constructor(private studentService: StudentService) {
  }

  ngOnInit(): void {
    this.studentService.studentForCourseChanged.subscribe(it => {
      this.students = it;
    });
  }

}
