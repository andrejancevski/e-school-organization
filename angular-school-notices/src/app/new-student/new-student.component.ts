import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {TeachersService} from '../services/teachers.service';
import {CourseTeacher} from '../models/CourseTeacher.model';
import {StudentService} from '../services/student.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-student',
  templateUrl: './new-student.component.html',
  styleUrls: ['./new-student.component.css', '../app.component.css'],
})
export class NewStudentComponent implements OnInit {
  newStudentForm: FormGroup;
  model: NgbDateStruct;
  courseTeachers: CourseTeacher[];

  constructor(private teacherService: TeachersService,
              private studentService: StudentService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.newStudentForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      studentEMBG: new FormControl(null, Validators.required),
      dateOfBirth: new FormControl(null, Validators.required),
      yearEnrolledIn: new FormControl(null, Validators.required),
      courses: new FormArray([]),
    });
    this.teacherService.getAllCoursesWithTeachers();
    this.teacherService.coursesTeachersChanged.subscribe(it => {
      this.courseTeachers = it;
    });
  }

  get controls(): any {
    return (this.newStudentForm.get('courses') as FormArray).controls;
  }

  formatDate(dateOfBirth): string {
    let month = dateOfBirth.month;
    let day = dateOfBirth.day;
    if (+dateOfBirth.day < 10) {
      day = '0' + dateOfBirth.day;
    }
    if (+dateOfBirth.month < 10) {
      month = '0' + dateOfBirth.month;
    }

    return day + '/' + month + '/' + dateOfBirth.year;
  }

  onSubmit(): void {
    const coursesForStudents = this.newStudentForm.value.courses.map(el => {
      const tempArray = el.subject.split(',');
      return {courseId: tempArray[0], teacherId: tempArray[1], teacherCode: tempArray[2]};
    });
    const newStudent = {
      studentName: this.newStudentForm.value.name,
      studentEMBG: this.newStudentForm.value.studentEMBG,
      studentSchoolYear: this.newStudentForm.value.yearEnrolledIn,
      studentDOB: this.formatDate(this.newStudentForm.value.dateOfBirth),
      coursesTeachers: coursesForStudents
    };
    this.studentService.createStudent(newStudent);
    this.router.navigateByUrl(`/admin/student/${newStudent.studentEMBG}`);
  }

  onSubjectAdded(): void {
    (this.newStudentForm.get('courses') as FormArray).push(
      new FormGroup({
        subject: new FormControl(null, Validators.required),
      })
    );
  }
}
