import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainHeaderComponent} from './main-page/main-header/main-header.component';
import {MainPageComponent} from './main-page/main-page.component';
import {MainDisplayComponent} from './main-page/main-display/main-display.component';
import {TeachersInfoComponent} from './main-page/teachers-info/teachers-info.component';
import {ParentsInfoComponent} from './main-page/parents-info/parents-info.component';
import {RegisterFormComponent} from './modals/register-form/register-form.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginFormComponent} from './modals/login-form/login-form.component';
import {TeacherPanelComponent} from './teacher-panel/teacher-panel.component';
import {NoticesComponent} from './notices/notices.component';
import {NoticeComponent} from './notices/notice/notice.component';
import {MeetingsComponent} from './meetings/meetings.component';
import {MeetingSlotComponent} from './meetings/meeting-slot/meeting-slot.component';
import {NewMeetingFormComponent} from './meetings/new-meeting-form/new-meeting-form.component';
import {AbsencesComponent} from './abscences/absences.component';
import {AbsenceSlotComponent} from './abscences/absence-slot/absence-slot.component';
import {NoticeFormComponent} from './notices/notice-form/notice-form.component';
import {StudentGroupsComponent} from './student-groups/student-groups.component';
import {StudentGroupComponent} from './student-groups/student-group/student-group.component';
import {StudentsComponent} from './student-groups/students/students.component';
import {StudentComponent} from './student-groups/students/student/student.component';
import {TeacherCoursesComponent} from './teacher-panel/teacher-courses/teacher-courses.component';
import {TeacherCourseComponent} from './teacher-panel/teacher-courses/teacher-course/teacher-course.component';
import {ParentPanelComponent} from './parent-panel/parent-panel.component';
import {StudentDetailComponent} from './student-detail/student-detail.component';
import {StudentGradesComponent} from './student-detail/student-grades/student-grades.component';
import {NewGradeFormComponent} from './student-detail/new-grade-form/new-grade-form.component';
import {NewAbsenceFormComponent} from './abscences/new-absence-form/new-absence-form.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {NewStudentComponent} from './new-student/new-student.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {PanelHeaderComponent} from './panel-header/panel-header.component';
import {AddTeacherCoursesComponent} from './add-teacher-courses/add-teacher-courses.component';
import {AddCoursesComponent} from './add-teacher-courses/add-courses/add-courses.component';
import {SelectedCoursesComponent} from './add-teacher-courses/selected-courses/selected-courses.component';
import {NoticeDetailComponent} from './notices/notice-detail/notice-detail.component';
import {ReigsterParentFormComponent} from './modals/reigster-parent-form/reigster-parent-form.component';
import {AuthInterceptorsService} from './interceptors/auth-interceptors.service';
import {FirstLoginComponent} from './first-login/first-login.component';

@NgModule({
  declarations: [
    AppComponent,
    MainHeaderComponent,
    MainPageComponent,
    MainDisplayComponent,
    TeachersInfoComponent,
    ParentsInfoComponent,
    RegisterFormComponent,
    LoginFormComponent,
    TeacherPanelComponent,
    NoticesComponent,
    NoticeComponent,
    MeetingsComponent,
    MeetingSlotComponent,
    NewMeetingFormComponent,
    AbsencesComponent,
    AbsenceSlotComponent,
    NoticeFormComponent,
    StudentGroupsComponent,
    StudentGroupComponent,
    StudentsComponent,
    StudentComponent,
    TeacherCoursesComponent,
    TeacherCourseComponent,
    ParentPanelComponent,
    StudentDetailComponent,
    StudentGradesComponent,
    NewGradeFormComponent,
    NewAbsenceFormComponent,
    AdminPanelComponent,
    NewStudentComponent,
    PanelHeaderComponent,
    AddTeacherCoursesComponent,
    AddCoursesComponent,
    SelectedCoursesComponent,
    NoticeDetailComponent,
    ReigsterParentFormComponent,
    FirstLoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorsService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}

