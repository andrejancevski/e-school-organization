import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../../services/student.service';
import {Observable, Subscription} from 'rxjs';
import {Grade} from '../../models/Grade.model';
import {PageGrade} from '../../models/PageGrade.model';

@Component({
  selector: 'app-student-grades',
  templateUrl: './student-grades.component.html',
  styleUrls: ['./student-grades.component.css', '../../app.component.css'],
})
export class StudentGradesComponent implements OnInit, OnDestroy {
  roleState = '';
  studentId = '';
  page = 1;
  collectionSize = 10;
  pageGrade: PageGrade;
  grades: Grade[];
  private subscription: Subscription;

  constructor(private router: Router, private activeRoute: ActivatedRoute, private studentService: StudentService) {
  }

  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');
    this.activeRoute.paramMap.subscribe(params => {
      this.studentId = params.get('id');
    });
    this.studentService.getGradesFromStudent(this.studentId, this.page, '10');
    this.subscription = this.studentService.gradeInPageChanged.subscribe(it => {
      this.pageGrade = it;
      this.collectionSize = it.totalPages * 10;
    });
  }

  onNewGradeAdded(): void {
    this.router.navigate(['teacher', 'student', this.studentId, 'newGrade']);
  }

  onPageChange($event: number): void {
    this.studentService.getGradesFromStudent(this.studentId, $event, '10');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
