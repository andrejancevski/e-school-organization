import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {Course} from '../../models/Course.model';
import {CoursesService} from '../../services/courses.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../../services/student.service';

@Component({
  selector: 'app-new-grade-form',
  templateUrl: './new-grade-form.component.html',
  styleUrls: ['./new-grade-form.component.css', '../../app.component.css'],
})
export class NewGradeFormComponent implements OnInit {

  @ViewChild('formElement') createGradeForm: NgForm;
  courses$: Observable<Course[]>;
  helper = new JwtHelperService();
  studentId = '';

  constructor(private courseService: CoursesService,
              private activatedRoute: ActivatedRoute,
              private studentService: StudentService,
              private router: Router) {
  }

  model: NgbDateStruct;

  ngOnInit(): void {
    const decodedToken = this.helper.decodeToken(localStorage.getItem('token'));
    this.activatedRoute.paramMap.subscribe(params => {
      this.studentId = params.get('id');
    });
    this.courses$ = this.courseService.getAllCoursesFromTeacher(decodedToken.sub);
  }

  getDateFormat(dateOfBirth): string {
    let month = dateOfBirth.month;
    let day = dateOfBirth.day;
    if (+dateOfBirth.day < 10) {
      day = '0' + dateOfBirth.day;
    }
    if (+dateOfBirth.month < 10) {
      month = '0' + dateOfBirth.month;
    }

    return day + '/' + month + '/' + dateOfBirth.year;
  }

  onSubmit(): void {
    const newStudentGrade = {
      courseId: this.createGradeForm.value.courseName,
      studentId: this.studentId,
      gradingPeriod: this.createGradeForm.value.gradingPeriod,
      examType: this.createGradeForm.value.typeOfGrade,
      grade: this.createGradeForm.value.grade,
      points: this.createGradeForm.value.points,
      gradeReview: this.createGradeForm.value.comment,
      forGPA: this.createGradeForm.value.forGPA !== ''
    };
    this.studentService.addGradeForStudent(newStudentGrade);
    this.router.navigate(['teacher', 'student', this.studentId, 'grade']);
  }
}
