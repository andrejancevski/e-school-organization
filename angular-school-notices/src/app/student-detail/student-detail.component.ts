import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../services/student.service';
import {Student} from '../models/Student.model';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css', '../app.component.css'],
})
export class StudentDetailComponent implements OnInit {
  roleState = '';
  studentId: string;
  student: Student;

  constructor(private router: Router, private activeRoute: ActivatedRoute, private studentService: StudentService) {
  }

  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');
    this.activeRoute.paramMap.subscribe(params => {
      this.studentId = params.get('id');
    });
    this.studentService.getStudentById(this.studentId);
    this.studentService.currentStudentLoaded.subscribe(it => {
      this.student = it;
    });
  }

  onGradeAdded(roleState: string): void {
    this.router.navigate(['student', this.studentId, 'grade'], {relativeTo: this.activeRoute.parent});
  }
}
