import {BackgroundColorService} from '../services/background-color.service';
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateUser} from '../models/CreateUser.model';
import {catchError} from 'rxjs/operators';
import {throwError, Observable, Subscription, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  helper = new JwtHelperService();
  errorThrown = new Subject<boolean>();


  constructor(private http: HttpClient, private router: Router, private activeRoute: ActivatedRoute, private modalService: NgbModal,
              private backgroundColorService: BackgroundColorService) {
  }

  logInUser(userName, password): void {
    this.http.post<any>('http://localhost:8080/authenticate', {userName: userName, password: password})
      .pipe(
        catchError(err => this.handleError(err))
      )
      .subscribe(it => {
        const decodedToken = this.helper.decodeToken(it.jwt);
        if (decodedToken.roles[0] === 'ROLE_ADMIN') {
          this.router.navigate(['admin'], {relativeTo: this.activeRoute});
        } else if (decodedToken.roles[0] === 'ROLE_TEACHER') {
          this.router.navigate(['teacher'], {relativeTo: this.activeRoute});
        } else {
          this.router.navigate(['parent'], {relativeTo: this.activeRoute});
        }
        localStorage.setItem('token', it.jwt);
        localStorage.setItem('role', decodedToken.roles[0]);
        this.backgroundColorService.setColorClass();
        this.modalService.dismissAll();
      });
  }

  createUser(newUser: CreateUser): void {
    this.http.post<CreateUser>('http://localhost:8080/api/user/create', newUser)
      .subscribe(it => {
        this.modalService.dismissAll();
      });
  }

  handleError(error): Observable<string> {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = 'Client side error' + error.error.message;
    } else {
      errorMessage = 'Server side error' + error.error.message;
    }
    this.errorThrown.next(true);
    return throwError(errorMessage);
  }


}
