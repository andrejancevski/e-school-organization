import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
    boolean | UrlTree {
    const token = localStorage.getItem('token');
    if (token) {
      if (localStorage.getItem('role') === 'ROLE_ADMIN') {
        return true;
      } else {
        return this.router.createUrlTree([this.resolveCurrentRole(localStorage.getItem('role'))]);
      }
    } else {
      return this.router.createUrlTree(['']);
    }
  }

  resolveCurrentRole(role: string): string {
    if (role === 'ROLE_TEACHER') {
      return 'teacher';
    } else if (role === 'ROLE_PARENT') {
      return 'parent';
    }
  }
}
