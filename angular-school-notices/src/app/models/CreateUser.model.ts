export interface CreateUser {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  teacherCode: string;
  studentsEMBG: number[];
  role: string;
}
