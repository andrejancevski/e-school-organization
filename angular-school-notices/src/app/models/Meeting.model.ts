export interface Meeting {
  meetingId: number;
  location: string;
  reason: string;
  dateTime: string;
  dateCreated: string;
  teacherOfTheMeeting: string;
  parentOfTheMeeting: string;
}
