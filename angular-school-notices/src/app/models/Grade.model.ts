export interface Grade {
  id: number;
  name: string;
  gradingPeriod: string;
  type: string;
  date: string;
  points: number;
  review: string;
  forGPA: boolean;
  courseName: string;
}
