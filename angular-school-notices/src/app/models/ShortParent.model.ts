export interface ShortParent {
  parentId: number;
  parentFirstName: string;
  parentLastName: string;
}
