export interface CourseTeacher {
  courseId: number;
  courseName: string;
  courseCredits: number;
  teacherId: number;
  teacherCode: string;
  teacherName: string;
  teacherLastName: string;
}
