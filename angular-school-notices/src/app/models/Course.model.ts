export interface Course {
  courseId: number;
  courseName: string;
  courseCredits: number;
}
