export interface NewMeeting {
  studentId: string;
  reasonForMeeting: string;
  location: string;
  dateTime: string;
}
