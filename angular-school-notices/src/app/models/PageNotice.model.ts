import {Notice} from './Notice.model';

export interface PageNotice {
  page: number;
  totalPages: number;
  pageSize: number;
  content: Notice[];
}
