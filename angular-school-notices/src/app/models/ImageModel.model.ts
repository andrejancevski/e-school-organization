export interface ImageModel {
  id: number;
  name: string;
  type: string;
  picByteArray: any;
}
