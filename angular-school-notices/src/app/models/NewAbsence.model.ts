export interface NewAbsence {
  studentName: string;
  startDate: string;
  endDate: string;
  absenceReason: string;
}
