export interface Absence {
  id: number,
  reason: string,
  startDate: string,
  endDate: string,
  absentStudent: string,
  parentName: string
}
