export interface Teacher {
  teacherId: number;
  teacherCode: string;
  teacherUserId: string;
  teacherFirstName: string;
  teacherLastName: string;
  teacherEmail: string;
}
