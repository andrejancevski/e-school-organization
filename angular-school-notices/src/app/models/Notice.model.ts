import {ImageModel} from './ImageModel.model';

export interface Notice {
  id: number;
  title: string;
  noticeText: string;
  importanceLevel: string;
  dateCreated: string;
  imageModel: ImageModel;
  user: string;
}


