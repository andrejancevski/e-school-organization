export interface NewStudent {
  studentName: string;
  studentEMBG: string;
  studentDOB: string;
  studentSchoolYear: string;
  coursesTeachers: [
    {
      courseId: number,
      teacherId: number,
      teacherCode: number
    }
  ];
}
