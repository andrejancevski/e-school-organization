export interface Student {
  studentEMBG: string;
  studentName: string;
  studentDOB: string;
  schoolYear: string;
  gpa: number;
  dateEnrolled: string;
  teachers: string[];
  courses: string[];
  parents: string[];
}
