import {NoticeDisplay} from './NoticeDisplay.model';

export interface PageNoticeDisplay {
  page: number;
  totalPages: number;
  pageSize: number;
  content: NoticeDisplay[];
}
