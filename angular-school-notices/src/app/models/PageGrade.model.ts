import {Grade} from './Grade.model';

export interface PageGrade {
  page: number;
  totalPages: number;
  pageSize: number;
  content: Grade[];
}
