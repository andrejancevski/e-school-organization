export interface NoticeDisplay {
  id: number;
  title: string;
  noticeText: string;
  importanceLevel: string;
  dateCreated: string;
  imageString: string;
  imageType: string;
  user: string;
}
