export interface NewGrade {
  courseId: number;
  studentId: string;
  gradingPeriod: string;
  examType: string;
  grade: string;
  points: number;
  gradeReview: string;
  forGPA: boolean;
}

