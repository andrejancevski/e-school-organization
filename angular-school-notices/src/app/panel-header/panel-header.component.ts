import {BackgroundColorService} from '../services/background-color.service';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Student} from '../models/Student.model';
import {StudentService} from '../services/student.service';

@Component({
  selector: 'app-panel-header',
  templateUrl: './panel-header.component.html',
  styleUrls: ['./panel-header.component.css'],
})
export class PanelHeaderComponent implements OnInit {
  roleState = '';
  students$: Observable<any>;

  constructor(private router: Router,
              private backgroundColorService: BackgroundColorService,
              private studentService: StudentService) {
  }

  ngOnInit(): void {
    this.roleState = localStorage.getItem('role');
    if (this.roleState === 'ROLE_PARENT') {
      this.students$ = this.studentService.getStudentFromParent();
    }
    this.backgroundColorService.setColorClass();
  }

  onLoggingOut(): void {
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
