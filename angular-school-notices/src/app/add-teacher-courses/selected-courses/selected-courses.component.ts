import {Component, OnDestroy, OnInit} from '@angular/core';
import {Course} from '../../models/Course.model';
import {TeachersService} from '../../services/teachers.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-selected-courses',
  templateUrl: './selected-courses.component.html',
  styleUrls: ['./selected-courses.component.css', '../../app.component.css']
})
export class SelectedCoursesComponent implements OnInit, OnDestroy {

  private coursesSubscription: Subscription;
  courses: Course[];
  selectedTeacherId: number;
  private selectedTeacherSubscription: Subscription;

  constructor(private teacherService: TeachersService) {
  }

  ngOnInit(): void {
    this.coursesSubscription = this.teacherService.selectedCoursesChanged.subscribe(it => {
      this.courses = it;
    });
    this.selectedTeacherSubscription = this.teacherService.selectedTeacher.subscribe(it => {
      this.selectedTeacherId = it;
    });
  }

  ngOnDestroy(): void {
    this.coursesSubscription.unsubscribe();
    this.selectedTeacherSubscription.unsubscribe();
  }

  removeCourse(course: Course): void {
    this.teacherService.removeCourse(course, this.selectedTeacherId);
  }
}
