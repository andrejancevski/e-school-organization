import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Teacher} from '../../models/Teacher.model';
import {Course} from '../../models/Course.model';
import {TeachersService} from '../../services/teachers.service';
import {CoursesService} from '../../services/courses.service';

@Component({
  selector: 'app-add-courses',
  templateUrl: './add-courses.component.html',
  styleUrls: ['./add-courses.component.css', '../../app.component.css']
})
export class AddCoursesComponent implements OnInit {

  courses$: Observable<Course[]>;

  // tslint:disable-next-line:no-input-rename
  @Input('teacher') teacher: Teacher;

  constructor(private teachersService: TeachersService, private courseService: CoursesService) {
  }

  ngOnInit(): void {
    this.courses$ = this.courseService.getAllCourses();
  }


  onCourseSelected(course: Course): void {
    this.teachersService.addCourse(course);
  }

}
