import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, of, Subject, Subscription} from 'rxjs';
import {Teacher} from '../models/Teacher.model';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {TeachersService} from '../services/teachers.service';
import {Course} from '../models/Course.model';
import {CoursesService} from '../services/courses.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-teacher-courses',
  templateUrl: './add-teacher-courses.component.html',
  styleUrls: ['./add-teacher-courses.component.css', '../app.component.css']
})
export class AddTeacherCoursesComponent implements OnInit, OnDestroy {
  private searchTerm = new Subject<string>();
  teachers$: Observable<Teacher[]>;
  disabled = false;
  @ViewChild('searchTeachersBox') teacherInput: ElementRef;
  selectedCourses: Course[];
  teacher: Teacher;

  private subscription: Subscription;

  constructor(private teachersService: TeachersService,
              private courseService: CoursesService,
              private router: Router,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.teachers$ = this.searchTerm.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(searchQuery => this.teachersService.getTeachersBySearchTerm(searchQuery))
    );
    this.subscription = this.teachersService.selectedCoursesChanged.subscribe(it => {
      this.selectedCourses = it;
    });

  }

  searchTeachers(searchQuery: string): void {
    this.searchTerm.next(searchQuery);
  }


  onTeacherSelected(teacher: Teacher): void {
    this.disabled = true;
    this.teacher = teacher;
    this.teacherInput.nativeElement.value = teacher.teacherFirstName + ' ' + teacher.teacherLastName;
    this.teachersService.selectedTeacher.next(teacher.teacherId);
    this.courseService.getAllCoursesFromTeacher(teacher.teacherEmail).subscribe(it => {
      this.teachersService.setSelectedCourses(it);
    });
    this.teachers$ = of([]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  reloadComponent(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/admin/courses']);
  }

  onAddCourses(): void {
    const addCoursesRequest = {
      teacherId: this.teacher.teacherId,
      courses: this.selectedCourses
    };
    this.teachersService.addAllCourses(addCoursesRequest);
    this.reloadComponent();
  }
}
