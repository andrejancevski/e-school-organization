import {LoginFormComponent} from './../login-form/login-form.component';
import {Router} from '@angular/router';
import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {OpenModalService} from '../open-modal.service';
import {Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../authentication/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit, OnDestroy {
  @ViewChild('content') content: ElementRef;
  closeResult = '';
  private subscription: Subscription;
  registeringState = ' ';

  constructor(private modalService: NgbModal,
              private omService: OpenModalService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.subscription = this.omService.modalOpen.subscribe(it => {
      this.registeringState = it;
      this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  findRole(): string {
    if (this.registeringState === 'admin') {
      return 'ROLE_ADMIN';
    } else if (this.registeringState === 'teacher') {
      return 'ROLE_TEACHER';
    }
  }

  onSubmit(registerForm: NgForm): void {
    const createUser = {
      firstName: registerForm.value.firstName,
      lastName: registerForm.value.lastName,
      email: registerForm.value.email,
      password: registerForm.value.password,
      teacherCode: this.registeringState === 'teacher' ? registerForm.value.teacherCode : '',
      studentsEMBG: [],
      role: this.findRole()
    };
    this.authService.createUser(createUser);
    this.router.navigate(['firstLogIn']);
  }
}
