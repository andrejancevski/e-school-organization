import {catchError} from 'rxjs/operators';
import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';
import {OpenModalService} from '../open-modal.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../authentication/auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import {BackgroundColorService} from 'src/app/services/background-color.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit, OnDestroy {

  @ViewChild('logInModal') logInModal: ElementRef;
  closeResult = '';
  private subscription: Subscription;
  private errorSubscription: Subscription;

  error = false;


  constructor(private modalService: NgbModal, private omService: OpenModalService,
              private authService: AuthService) {
  }

  ngOnInit(): void {

    this.subscription = this.omService.logInModalOpen.subscribe(it => {
      this.modalService.open(this.logInModal, {ariaLabelledBy: 'modal-login-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    });
    this.errorSubscription = this.authService.errorThrown.subscribe(it => {
      this.error = it;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onSubmit(logInForm: NgForm): void {
    this.authService.logInUser(logInForm.value.email, logInForm.value.password);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.errorSubscription.unsubscribe();
  }
}
