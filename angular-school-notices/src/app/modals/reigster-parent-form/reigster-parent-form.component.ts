import {LoginFormComponent} from './../login-form/login-form.component';
import {distinctUntilChanged} from 'rxjs/operators';
import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OpenModalService} from '../open-modal.service';
import {AuthService} from '../../authentication/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reigster-parent-form',
  templateUrl: './reigster-parent-form.component.html',
  styleUrls: ['./reigster-parent-form.component.css']
})
export class ReigsterParentFormComponent implements OnInit, OnDestroy {

  @ViewChild('registerParentModal') registerParentModal: ElementRef;
  newParentForm: FormGroup;
  closeResult = '';
  private subscription: Subscription;

  constructor(private modalService: NgbModal,
              private omService: OpenModalService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.newParentForm = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      children: new FormArray([]),
    });
    this.subscription = this.omService.registerParentModal.subscribe(it => {
      this.modalService.open(this.registerParentModal, {ariaLabelledBy: 'modal-register-parent-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        (this.newParentForm.get('children') as FormArray).clear();
      });
    });
  }

  get controls(): any {
    return (this.newParentForm.get('children') as FormArray).controls;
  }

  onChildAdded(): void {
    (this.newParentForm.get('children') as FormArray).push(
      new FormGroup({
        childEMBG: new FormControl(null, Validators.required),
      })
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  onSubmit(): void {
    const createUser = {
      firstName: this.newParentForm.value.firstName,
      lastName: this.newParentForm.value.lastName,
      email: this.newParentForm.value.email,
      password: this.newParentForm.value.password,
      teacherCode: '',
      studentsEMBG: this.newParentForm.value.children.map(it => {
        return it.childEMBG;
      }),
      role: 'ROLE_PARENT'
    };
    this.authService.createUser(createUser);
    this.router.navigate(['firstLogIn']);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
