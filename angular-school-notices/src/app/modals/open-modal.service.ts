import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpenModalService {

  modalOpen = new Subject<string>();
  logInModalOpen = new Subject<boolean>();
  registerParentModal = new Subject<boolean>();

  constructor() {
  }

  openLogInModal(): void {
    this.logInModalOpen.next(true);
  }

  openParentModal(): void {
    this.registerParentModal.next(true);
  }

  openTeacherModal(): void {
    this.modalOpen.next('teacher');
  }

  openAdminModal(): void {
    this.modalOpen.next('admin');
  }

}
