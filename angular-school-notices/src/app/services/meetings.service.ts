import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewMeeting} from '../models/NewMeeting.model';
import {Observable} from 'rxjs';
import {Meeting} from '../models/Meeting.model';

@Injectable({
  providedIn: 'root'
})
export class MeetingsService {

  constructor(private http: HttpClient) {
  }

  createNewMeeting(meeting: NewMeeting): void {
    this.http.post('http://localhost:8080/api/meeting/create', meeting).subscribe(it => {
    });
  }

  getAllMeetings(): Observable<Meeting[]> {
    return this.http.get<Meeting[]>('http://localhost:8080/api/meeting/allMeetings');
  }
}
