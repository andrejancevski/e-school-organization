import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Course} from '../models/Course.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  URL = 'http://localhost:8080/api/course/';

  constructor(private http: HttpClient) {
  }

  getAllCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.URL + 'allCourses');
  }

  getAllCoursesFromTeacher(teacherEmail: string): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.URL}coursesTeacher?teacherEmail=${teacherEmail}`);
  }

  getAllCoursesFromStudent(studentId: string): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.URL}student/${studentId}`);
  }

}
