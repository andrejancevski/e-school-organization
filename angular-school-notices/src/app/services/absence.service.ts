import {Injectable} from '@angular/core';
import {NewAbsence} from '../models/NewAbsence.model';
import {Absence} from '../models/Absence.model';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AbsenceService {

  URL = 'http://localhost:8080/api/absence/';

  absences = new Subject<Absence[]>();

  constructor(private http: HttpClient) {
  }

  createNewAbsence(newAbsence: NewAbsence): void {
    this.http.post(this.URL + 'newAbsence', newAbsence).subscribe(it => {
    });
  }

  getAbsences(): void {
    this.http.get<Absence[]>(this.URL + 'getAll')
      .subscribe(it => {
        this.absences.next(it);
      });

  }
}
