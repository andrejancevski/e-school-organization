import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BackgroundColorService {
  roleState = '';

  constructor() {
  }

  setColorClass(): void {
    this.roleState = localStorage.getItem('role');
    if (this.roleState === 'ROLE_ADMIN') {
      document.documentElement.style.setProperty('--background-color-main', 'var(--color-admin)');
      document.documentElement.style.setProperty('--color-main-lighter', 'var(--color-admin-lighter)');
    } else if (this.roleState === 'ROLE_TEACHER') {
      document.documentElement.style.setProperty('--background-color-main', 'var(--color-teacher)');
      document.documentElement.style.setProperty('--color-main-lighter', 'var(--color-teacher-lighter)');
    } else if (this.roleState === 'ROLE_PARENT') {
      document.documentElement.style.setProperty('--background-color-main', 'var(--color-parent)');
      document.documentElement.style.setProperty('--color-main-lighter', 'var(--color-parent-lighter)');
    }
  }

}
