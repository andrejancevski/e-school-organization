import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ShortParent} from '../models/ShortParent.model';

@Injectable({
  providedIn: 'root'
})
export class ParentsService {

  constructor(private http: HttpClient) {
  }


  getParentsFromStudent(studentId: string): Observable<ShortParent[]> {
    return this.http.get<ShortParent[]>(`http://localhost:8080/api/parents/student/${studentId}`);
  }
}
