import {Injectable} from '@angular/core';
import {NewStudent} from '../models/NewStudent.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NewGrade} from '../models/NewGrade.model';
import {Observable, Subject} from 'rxjs';
import {ShortStudent} from '../models/ShortStudent.model';
import {Student} from '../models/Student.model';
import {PageGrade} from '../models/PageGrade.model';


@Injectable({
  providedIn: 'root'
})
export class StudentService {
  URL = 'http://localhost:8080/api/';

  studentsForGivenCourse: ShortStudent[];

  studentForCourseChanged = new Subject<ShortStudent[]>();

  currentStudent: Student;

  currentStudentLoaded = new Subject<Student>();

  gradeInPage: PageGrade;

  gradeInPageChanged = new Subject<PageGrade>();


  constructor(private http: HttpClient) {
  }

  createStudent(student: NewStudent): void {
    this.http.post(this.URL + 'student/create', student).subscribe(it => {
    });
  }

  addGradeForStudent(newGrade: NewGrade): void {
    this.http.post(this.URL + 'grade/newGrade', newGrade).subscribe(it => {
    });
  }

  getStudentFromParent(): Observable<any> {
    return this.http.get(`${this.URL}parents/parentStudents`);
  }

  getStudentsFromCourse(courseId: number): void {
    this.http.get<ShortStudent[]>(`${this.URL}course/students?courseId=${courseId}`).subscribe(it => {
      this.studentsForGivenCourse = it;
      this.studentForCourseChanged.next(this.studentsForGivenCourse);
    });
  }

  getStudentById(studentId: string): void {
    this.http.get<Student>(`${this.URL}student/${studentId}`).subscribe(it => {
      this.currentStudent = it;
      this.currentStudentLoaded.next(this.currentStudent);
    });
  }

  getGradesFromStudent(studentId: string, page: number, size: string): void {
    let headers = new HttpHeaders();
    headers = headers.set('page', (page - 1).toString());
    headers = headers.set('page-size', size);
    this.http.get<PageGrade>(`${this.URL}grade/${studentId}`, {headers: headers}).subscribe(it => {
      this.gradeInPage = it;
      this.gradeInPageChanged.next(it);
    });
  }
}
