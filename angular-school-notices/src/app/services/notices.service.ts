import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {Notice} from '../models/Notice.model';
import {map} from 'rxjs/operators';
import {PageNotice} from '../models/PageNotice.model';
import {PageNoticeDisplay} from '../models/PageNoticeDisplay.model';
import {NoticeDisplay} from '../models/NoticeDisplay.model';

@Injectable({
  providedIn: 'root',
})
export class NoticesService {
  URL = 'http://localhost:8080/api/notice/';

  noticeDetail = new Subject<NoticeDisplay>();

  pageNoticeLoaded = new Subject<PageNoticeDisplay>();

  constructor(private http: HttpClient) {
  }

  getAllNotices(page: number, size: number): void {
    let headers = new HttpHeaders();
    headers = headers.set('page', (page - 1).toString());
    headers = headers.set('page-size', size.toString());
    this.http
      .get<PageNotice>(this.URL + 'allNotices', {headers: headers})
      .pipe(map((n) => this.createNoticeDisplayObject(n)))
      .subscribe((it) => {
        this.pageNoticeLoaded.next(it);
      });
  }

  createNoticeDisplayObject(pageNotice: PageNotice): PageNoticeDisplay {
    return {
      page: pageNotice.page,
      pageSize: pageNotice.pageSize,
      totalPages: pageNotice.totalPages,
      content: pageNotice.content.map((notice) => {
        return {
          id: notice.id,
          title: notice.title,
          noticeText: notice.noticeText,
          importanceLevel: notice.importanceLevel,
          dateCreated: notice.dateCreated,
          imageString:
            'data:' +
            notice.imageModel.type +
            ';base64,' +
            notice.imageModel.picByteArray,
          imageType: notice.imageModel.type,
          user: '',
        };
      }),
    };
  }

  createNewNotice(formData: FormData): void {
    this.http.post(this.URL + 'create', formData).subscribe((it) => {
    });
  }

  createNoticeDetailDisplayObject(noticeDetail: Notice): NoticeDisplay {
    return {
      id: noticeDetail.id,
      title: noticeDetail.title,
      noticeText: noticeDetail.noticeText,
      importanceLevel: noticeDetail.importanceLevel,
      dateCreated: noticeDetail.dateCreated,
      imageString:
        'data:' +
        noticeDetail.imageModel.type +
        ';base64,' +
        noticeDetail.imageModel.picByteArray,
      imageType: noticeDetail.imageModel.type,
      user: noticeDetail.user,
    };
  }

  getNoticeById(id: string): void {
    this.http
      .get<Notice>(`${this.URL}${id}`)
      .pipe(map((n) => this.createNoticeDetailDisplayObject(n)))
      .subscribe((it) => {
        this.noticeDetail.next(it);
      });
  }

  deleteNoticeById(id: number): void {
    this.http
      .delete(`${this.URL}${id}`, {
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      })
      .subscribe();
  }
}
