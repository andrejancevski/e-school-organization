import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {Teacher} from '../models/Teacher.model';
import {HttpClient} from '@angular/common/http';
import {Course} from '../models/Course.model';
import {CourseTeacher} from '../models/CourseTeacher.model';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {
  URL = 'http://localhost:8080/api/';

  selectedCourses: Course[] = [];

  selectedCoursesChanged = new Subject<Course[]>();

  coursesTeachersChanged = new Subject<CourseTeacher[]>();

  coursesTeachers: CourseTeacher[];

  selectedTeacher = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  getTeachersBySearchTerm(searchQueryString: string): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(`${this.URL}teacher?name=${searchQueryString}`);
  }

  setSelectedCourses(courses: Course[]): void {
    this.selectedCourses = courses;
    this.selectedCoursesChanged.next(this.selectedCourses);
  }

  addCourse(course: Course): void {
    if (!this.checkIsCourseAdded(this.selectedCourses, course)) {
      this.selectedCourses.push(course);
      this.selectedCoursesChanged.next(this.selectedCourses);
    }
  }

  removeCourse(course: Course, selectedTeacherId: number): void {
    this.selectedCourses = this.selectedCourses.filter(it => it !== course);
    this.selectedCoursesChanged.next(this.selectedCourses);
    const courseToRemove = {
      teacherId: selectedTeacherId,
      courseId: course.courseId
    };
    this.http.request('delete', this.URL + 'teacher/removeCourse', {body: courseToRemove})
      .subscribe(it => {});
  }

  addAllCourses(addCoursesRequest: any): void {
    this.http.post(this.URL + 'teacher/addCourses', addCoursesRequest).subscribe(it => {
    });
  }

  checkIsCourseAdded(courses: Course[], course: Course): any {
    for (const c of courses) {
      if (c.courseId === course.courseId) {
        return true;
      }
    }
    return false;
  }

  getAllCoursesWithTeachers(): void {
    this.http.get<CourseTeacher[]>(this.URL + 'course/coursesTeachers').subscribe(
      it => {
        this.coursesTeachers = it;
        this.coursesTeachersChanged.next(this.coursesTeachers);
      }
    );
  }
}

